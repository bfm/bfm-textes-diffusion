
 [...] alte [...] 
Cist ot nun Colgrim le sage;
Mult esteit defiere lignage.
Pois se asemblerent li Bretun
Pur la mortUterpendragon 
En Leïrcestre la cyté,
Qui est faite deantiquité.
Danz Dubrices i est mandé,
Qui fu chief de lacrestïenté,
Arcevesque de Kaerlïun,
Uns hoem de grantreligïun.
Prient li par humilité
Que des Bretuns eüstpieté
E le meschin Artur coronast,
E a rei leconsacerast ;
Car a dreit rei deut estre,
Si cum furent siancestre.
Mult en iert grant necessité
A tuz les baronsdel régné,
Car paien grant gent esteient
Qui lur terespurpreneient,
E pur plus gent enveioent
Que dëultremer veneient.
Dubricius si en ot pieté.
Les evesques adasemblé ;
Si ad Artur a rei sacré,
De corone de or le adcoroné.
Li Bretun qui i ierent venu
Si home sunttuit devenu.
A seinur l'unt tuit receü,
E lur fius de li unttenu.
Quinze anz ot Artur a icel tens;
De sun eé multot grant sens.
De tote gent mult fu amé,
Mult preisié emult alosé.
Unc ne vit home de sun eage
Ne damisel quetant fu sage.
Preuz iert a desmesure,
Mult alignié e debele nature.
Une bone tecche reaveit
Dunt il multpreisié esteit:
Que il donot mult volentiers
Quanqu'il poeit achevaliers ;
E quant rien n'aveit a doner,
Mult li pesat delrefuser;
E dulcement bien lur prometot;
Tut ceo qu'ilpoeit lur donot.
A celi ne poet faillir honur
Qui tut tens estsi doneür.
Ja poverté nel constreinderat,
Mes sun bienli acreisterat.
Eissi avint al rei Artur
Que tuz jorz li crutses honur;
E cum plus largement donot,
E plus aveir liamontot.
Desormés se purpensat
Que tel chosecomencerat,
Dunt il les soens aricheierat
E il alosé multserat.
Retenu ad riches maeisnees,
Qu'il mult bel adapareillees.
La juvencele del païs
Tute est od le reiremis.
Par grant proësce sis venqui.
Li cuens Colgrins si fudvencu ;
Des soens i ad forment perdu.
A Everwic le ad chascié
E iloec dedenz asiegié.
Tuz les quidat aver suppris.
Mes dans Colgrins un frereaveit :
Icil Baldulf apelé esteit,
Qui sur la marineatendeit 
Le rei Cherdi qui venir deveit.
Baldulf entendit la novele
Que pas ne li sembla bele:
Que sun frere esteit asiegé
En Everwic la bone cité;
Venir deveit cum plus tostsot
Od tote la force que il pot.
A Everwic Baldulfala,
Sis milie homes od sei mena,
En embuschage suntherbergié 
A dis liwes de la cité.
Par nuit voleit Baldulferrer,
E dedenz Everwic entrer.
Cum a Artur ceo futnuncié
Mult tost ad Cador apelé
Que li nies sunfrere esteit;
Tote Cornewaille en baillie aveit.
Icil cum sis cenz chevaliers,
Qui preuz furent e hardiz e fiers,
Od les paiens suntencontré,
E si bien les unt escrïé
Que des soens n'untnenil oscis;
Li altre en fuie se sunt mis.
Li cuens Baldulf s'esteschapez,
Qui forment s'est purpensez
Coment se poeitcunseillier 
Que od sun frere peusset parler
E entendre queceli diseit,
Que par le sen que il aveit
Par engin eschaperpoeit;
Par sun grant sen bien le guareit.
E com ne vit altreretur,
Si se feinst estre jugleür:
Pur ceo ad sa barbetondi,
E une harpe a sun col pendi.
Envers l'ost Artur s'esttorné;
Dedenz cel ost s'en est entré,
Si sus e jus aladharpant 
E les sonez alad chantant.
Quant si long tens i addemeuré 
Que il fu de tuz ja aseuré,
A la cité adacenez,
E a cels dedenz acointez:
Si furent tuitmult liez.
Cordes qu'il eurent li unt getez;
Dedenz la cité l'unttost trait.
Sis freres grant joië en feit,
L'ad enbracié e multgabbé.
De altre part li est nuncié
Que en Escoce est arivez
Cherdic li reis od sis cenz nefs.
Né fu de une soer Artur;
Budicius le ot aguinur.
Li rei Hoël en est tut lié;
Si ad sa gent mult tost mandé,
Mult tost entré en ses nefs:
Od quinze mile de chevaliers
Par halte mer vont siglant.
Vent unt mult bon e cillant;
Al tierz jor od lur esforz
A Hamtone arivent dreit as porz.
Quant Artur oi que il est venu,
Od grant joie l'ad receü;
Od sei a Lundres l'enmena,
E mult grant joie i demena.
Pois ad sun ost tut asemblé,
E a Nicole sunt tost alé
Car paien l'eurent asiegié,
E cels dedenz unt mult grevé.
Od els se sunt combatu;
La merci Deu sis unt vencu.
Bien sis mile en sunt periz,
Que de neiez, que de oscis;
Le siege en unt paien guerpi
E vers la mer s'en sunt fuï.
Li Breton les vont chasçant
Desque a un bois qui mult fu grant;
Iloec sunt paien aresté,
Des Crestiens unt mulz tué.
Cum ceo vit li reis Artur,
Le siege fist mettre tut entur.
Defors fist le bois tut plaeissir
Tut envirun pur els honir;
Ses eschieles pois i mist:
Treis jors pleniers les asist.
Quant li paien n'unt que mangier
Ne nule part ne poent eschaper,
A Artur unt dunc enveié
E tuit li unt merci crïé:
Si li donent mult hastivement
Tut lur or e lur argent,
E quanque en la terre aveient
Fors lur nefs qu'il teneient,
E lur treü li unt nomé,
E bons ostages en unt trové
Que il tost li envëereient
De altre mer cum la vendreient
Si cum il le eurent purparlé.
Li reis Artur lur ad otreié,
Car bien s'en est aseüré
Que li soens li ont conseillé.
Li paien s'en sunt tuit alé
E en lur nefs se sunt entré;
Mes cum il sunt en halte mer
Si se prenent a purpenser,
E dient tuit comunalement
Que mult l'unt feit malveisement
Quant a Artur unt treu asis,
E lur ostages i unt mis.
Tuit dient qu'il returnerunt:
Ja pur ostage nel larunt.
En halte mer sunt returné,
E en Toteneis sunt arivé.
La guere unt tost comencee:
Tote deguastoent la contree,
 [...] das [...] lle [...] 
 [...] ever dunc li convendreit
Ceo que en propos avant aveit,
Car tres bien en corage aveit
De tut en tut qu'il destruereit
E lesPicteis e les Escoz,
E les paens e tuz lur esforz.
Mes or li covient returner
Pur ses francs homes delivrer,
E pur sucurre sa cité
Que de paens iert asiegé.
De altre part poür aveit
Del rei Hoël qui remaneit
En Alclud, que si malade esteit
Que il errer pas ne poeit.
Li reis od sun host tant alat
Que dedenz Sumersate entrat,
E que il bien le siege vit.
A tuz les soens pois ad dit:
« Or voil, seigneurs, que vus sachiez
Que al paien que vus vëez
Envers mei unt mult mespris,
Car parjuré sunt cil enemis.
Si Deu pleist, oi m'en vengerai,
Car od els me combaterai.
Or as armes hastivement,
Si l'asaillum vistement! »
Si cum descendirent li Bretun,
E li arcevesque de Kaerlïun
Sus un munt est munté,
A halte voiz ad sermoné:
« Or escultez, gentil baron!
Cil Deu vus doinst sa beneiçun
Qui par angele fu anuncié
A virgine Marie, e de li né!
Iceo vus pri tuz, pur Deu amur,
Que entre vus seit paeis e dulçur,
E meintenez crestienté.
De vus e des vos aiez pieté;
Pur vos prohesmes combatez,
E vostre pais si defendez,
Car tuit cil qui ci morunt
Tut pur veir a Deu irrunt.
Des paens ja mar aiez pour!
Sovienge vus de icel Seignur
Que tut le mund format!
Adam e Eve de tere criat;
Del deluvie guarid Noé,
Par tant qu'il fist sa volenté;
A Abraham qu'il mult amat
La circumcisïon mustrat.
A Moÿsen, qui fut de fei,
El munt Syna dona la Lei.
Le prophete pois enveiat,
Qui sa naeissance prophetizat.
De la virgine pois fu né,
E par esteille demustré;
Des treis reis fu bel ahoré,
E el seint Temple pois fu présenté
A duze anz; fist de l'ewe vin,
Del quel but li rei Archetriclin;
Pois cum trent anz ot passé
El flum Jordan fut baptizié.
Sur li vint li Seint Espirit
Cum colomb, si li dist
Que il veir Fiz Deuesteit»
