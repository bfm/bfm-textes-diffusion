LOIS DE GUILLAUME LE CONQUÉRANT
Cez sunt les leis e les
custumes que li reis Will.
grantad al pople de Engleterre
apres le cunquest de
la terre; iceles meimes
que li reis Edward, sun
cusin, tint devant lui.
Ceo est a saver :
Pais a seinte iglise. De
quel forfeit que hom oust
fet, e il poust venir a
seinte iglise, oust pais
de vie e de menbre.
E si aucuns meist
main en celui ki la mere
iglise requereit, si ceo
fust u evesqué u abeie u
iglise de religiun, rendist
ceo qu'il avreit pris, e
cent souz le forfeit; e de
mere iglise de parosse
xx souz; e de chapele x
souz.
E ki enfreint la
pais le rei, en Merchenelahe
cent souz les amendes.
Autresi dehemforeet de
agwait purpensé. Icel plait
afert a la curune le rei.
E si aucuns
vescunte u provost mesfait
as humes de sa baillie,
e de ceo seit ateint
devant justise, le forfeit
est a duble de
ceo que autre fust forfeit.
E ki en Denelahe
enfreint la pais le rei, set
vint livres e quatre les
amendes e les forfez le
rei, ki aferent al vescunte,
Lsouz. en Merchenelahe
exlsouz en
Westsexenelahe.
E cil francs
hom ki ad e sache e
soche e toll e tem e infangentheof,
se il est enplaidé,
e il seit mis en forfeit
el cunté, afert al os
le vescunte en Denelahexl
ores, e de cel hume ki
ceste franchise nen ad
xxxii ores. De cez xxxii
ores averad le vescunte
al os le rei x ores, e
cil ki le plait averad
deredné vers lui xii ores,
e le seinur, en ki fiu il
meindra, les x ores. Ceo
est en Denelahe.
La custume en
Merchenelahe est : si aucuns
est apelé de larrecin
u de roberie, e il seit
plevi a venir devant justise,
e il s'en fuie dedenz
[le terme], sun plege si
averad terme de un
meis e un jur de querre le;
e s'il le pot truver dedenz
le terme, sil merra a la justise;
e s'il nel pot truver,
si jurra sei duzime main,
que, a l'ure qu'il le plevi,
larrun nel sout, ne par
lui ne s'en est fuid, ne
aver nel pot.
Dunc rendrad le
chatel, dunt il est retez, e
xx souz pur la teste e iiii
den. al ceper, e une maille
pur la besche, exlsol. al
rei.
E en Westsexenelahe
c sol.; xx sol. al clamif
pur la teste e iiii lib.
al rei.
En Denelahe viii
lib. le forfeit : les xx sol.
pur la teste, les vii lib. al
rei.
E s'il pot dedenz
un an e un jur truver le
larrun e amener a justise,
si lui rendra cil les xx sol.
kis averad oud e sin
ert feite la justise del
larrun.
Cil ki prendra larrun
senz siwte e senz
cri que cil enlest a ki il
avera le damage fait, e il
vienge apres, si est resun
qu'il duinse x sol de
hengwite e si face la justise
a la primere devise.
E s'il passe la devise senz
le cunged a la justise, si
est forfeit dexlsol.
Cil ki aveir rescut,
u chevals u bos u vaches
u berbiz u pors, que est
forfengapelé en engleis, cil
kis claimed durrad al
provost pur la rescussiun
viii den ; ja tant n'i
ait, mes qu'il i oust cent
almaille, ne durrad que viii
den.; e pur un porc i
den., e pur i berbiz i
den., e issi tresque a
viii, pur chascune i den.;
ne ja tant n'i averad, ne
durrad que viii den.. E
durrad gwage e truverad
plege, que si autre vienge
aprof dedenz l'an e le
jur pur l'aveir demander,
qu'il l'ait a dreit en la
curt celui ki l'aveit rescus.
Autresi de aveir
adiré e de autre truveure,
seit mustred de treis parz
deI visned, qu'il ait testimonie
de la truveure. E
si aucuns vienged avant
pur clamer la chose,
duinst gwage e truist
plege, que si autre
le cleimt dedenz l'an e
un jur, qu'il l'ait a dreit
en la curt celui ki l'avera
truved.
Si hom ocist autre,
e il seit cunuissant e
il deive faire les amendes,
durrad de sa manbote al
seinur pur le franch hume
x sol. e pur le serf xx sol.
La were del thein
xx lib.en Merchenelahe,
xxv lib. en Westsexenelahe;
e la were del vilain c sol.
en Merchenelahe e ensement
en Westsexenelahe.
Primereinement
rendrad l'om del halsfang
a la vedve x sol., e le surplus
les parenz e les orfenins
partent entre eus.
En la were purra il rendre
cheval ki ad la coille
pur xx sol., e tor pur x
sol., e ver pur v sol.
Si hom fait plaie
en autre, e il deive faire
les amendes, primereinement
lui rende sun lecheof;
e li plaez jurra sur
seinz que pur meins
nel pot feire ne pur haur
si cher nel fist.
Si la plaie lui vient
el vis en descuvert, al
pouz tuteveies viii den., u
en la teste u en autre liu u
ele seit cuverte, al pouz tuteveies
iiii den. E de tanz
os cum l'om trait de la
plaie, al os tuteveies iiii
den.
Puis al acordement,
si lui metera avant
honurs, e jurra que s'il
lui oust fait ceo qu'il
lui ad fet, e se sun
quor lui purportast e
sun cunseil lui dunast,
prendreit de lui ceo que
offert ad a lui.
Si ceo avient
que aucuns coupe le
puing al autre u le pie,
si lui rendrad demi were
sulunc ceo qu'il est nez.
Del poucer lui rendra
la meité de la main; del
dei apres le poucer xv
sol. de sol. engleis, que
est apelé quaer denier;
del lung dei, xvi sol.; del
autre, ki porte l'anel,
xvii sol.; del petit dei, v
sol.; del ungle, s’il le
couped de la charn, v
sol. de souz Engleis; al
ungle del petit dei, iiii den.
Cil ki autrui
femme purgist, si forfeit
sun were vers sun seinur.
Autresi ki faus
jugement fait, pert sa were,
s'il ne pot jurer sur seinz
que mieuz nel sout juger.
Si hom apeled
autre de larrecin, et il
seit franchs hom et il
puissed aver testimonie de
lealted, se escundirad par
plein serment; et autre ki
blasmé ait esté, se escundirad
par serment numé,
ceo est a saver per xiiii
humes leals par num,
s'il les pot aver; si s'en
escundira sei duzime
main. E si il aver nes pot,
si s'en defende par juise;
e li apelur jurra sur lui par
vii humes numez, sei siste
main, que pur haur nel fait
ne pur autre chose, se pur
sun dreit nun purchacer.
E si aucuns est
apeled de mustier fruisser
u de chambre, e il n'ait
esté en ariere blasmé, s'en
escundisse par xiiii humes
leals numez, sei duzime
main. E s'il ait autre
fiede esté blasmé, s'en
escundisse a treis duble,
ceo est a saveir parxlii
leals humes numez, sei
trente siste main. E s'il
aver nes pot, aut a la
juise a treis duble, si cum
il deust a treis duble
serment. E s'il ad larrecin
ca en ariere amendé, aut
al ewe.
Li ercevesque averad
de forfeirurexlsol. en
Merchenelahe, e li eveske
xx sol. et li queons xx
sol. e li barun x sol. e
li sochemanxlden.
Cil ki ad aveir
champestre xxx den. vaillant
deit duner le den.
Seint Piere. Le seignur
pur un den. que il donrad,
si erent quites ses bordiers
e ses bovers e ses serjanz.
Li burgeis qui ad en soun
propre chatel demi marc
vailant deit doner le dener
Seint Pere.
Ki en Denelahe
franch hume est, s'il ad
demi marc vaillant d'aveir
champestre, si duinst le
den. Seint Piere; e par le
den. que li sire durrad,
si erent quite cil ki
meindrunt en sun demeine.
Ki retient le
dener Seint Pere, le dener
rendra per la justice de
seinte eglise e xxx den.
de forfait.
E si il est enplaidé
de la justise le rei,
le forfait al evesque xxx den.
e al reiXLsolz.
Cil ki purgist
femme a force, forfeit ad
les menbres. Ki abat
femme a terre pur fere lui
force, la multe al seinur x
sol.
Si alquns crieve
l'oil al altre per aventure
quel que seit, si amendrad
LXXsolz de solz engleis, e
si la purnele i est remis, si
ne rendra lui que la meité.
De relief a
cunte, ki al rei afert: viii
chevals enfrenez e enseelez
les iiii, e iiii haubercs
e iiii haumes e iiii escuz
e iiii lances e iiii espees.
Les autres, ii chaceurs e ii
palefreis a freins e a chevestres.
De relief a barun :
iiii chevals, les ii enfrenez e
enseelez, e ii haubercs, e ii
haumes, e ii escuz, e ii
espees e ii lances. E les
autres ii chevals, un chaceur
e un palefrei a freins e
a chevestres.
De relief a vavassur
a sun lige seinur :
deit estre quite par le cheval
sun pere, tel cum il
out le jur de sa mort, e par
sun haume e par sun escu e
par sun hauberc e par sa
lance e par s'espee. E s'il
fust desaparaillé, qu'il
n'oust cheval ne armes,
fust quite par c sol.
De relief a vilain:
le meillur aveir qu'il averad,
u cheval u bof u vache,
durrad a sun seinur, e
puis seient tuz les vilains en
franc plege.
Cil qui tenent lur
terre a cense, soit lur dreit
relief a tant cum la cense
est de un an.
De entercement de
vif aveir, kil voldra clamer
pur embled e voldrad duner
gwage e truver plege
a parsivre sun apel,
dunc estuvera a celui
ki l'avera entre mains
numer sun guarant, s'il
l'ad.
E s'il ne l'ad, dunc
numerad il sun heimelborch
e ses testimonies,
e ait les a jur e a terme,
s'il les ad; e li enterceur le
mettrad en guage sei
siste main, e li autre le
mettrad en la main sun
guarant, u a sun heimelborch,
lequel qu'il averad.
E s'il n'ad guarant ne haimelborch,
e il ait les testimonies
qu'il le achatad al
marché le rei, e qu'il ne
set sun guarant ne
sun plege vif ne mort,
ceo jurrad od ses testimonies
od plein serment,
si perdera sun chatel,
s’il testimonient qu'il
heimelborch en prist.
E s'il ne pot guarant
ne testimonie aver, si perderad
e parsoudrad e pert sun
were vers sun seinur. Ceo
est en Merchenelahe e en Denelahge.
En Westsexenelahge
ne vocherad il mie sun
guarant devant iceo qu'il
seit mis en guage. En Denelahe
mettrad l'om l'aveir
en uele main, de ici qu'il
seit derehdned; e s'il pot prover,
que ceo seit de sa nureture,
par de treis parz
de sun visned, si l'averad
derehdné; kar puis que
le serment lui est juged ne
l'en pot l'om puis lever par
le jugement de Engleterre.
Ki Franceis
ocist, e les humes del hundred
nel prengent e meinent
a la justise dedenz les
viii jurs, pur mustrer
k'il l'ait fet, si renderunt
le murdre xlvi
mars.
Si hom volt derehdner
cuvenant de terre
vers sun seinur, par ses
pers de la tenure meimes,
qu'il apelerad a testimonie,
lui estuverad derehdner,
kar par estrange
nel purrad pas derehdner.
De hume ki
plaided en curt, en ki
curt que ceo seit, fors la
u le cors le rei seit, e
hom lui met sure
k'il ad dit chose qu'il ne
voille conuistre, s'il pot
derehdner par un entendable
hume del plait, oant et
veant, qu'il ne l'averad
dit, recovré ad sa parole.
De quatre chemins,
ceo est a saveir Watlingestrete,
Ermingestrete,
Fosse, Hykenild , ki en
aucun de ces quatre chemins
ocist aucun ki seit
errant par le pais, u assaut,
si enfreint la pais le rei.
Si larrecin est
truved, en ki terre que ceo
seit, e le larrun ovoc, li
seinur de la terre e la
femme averunt la meité
deI aveir al larrun, e les
chalenjurs lur chatel,
s'il le trovent; e l'autre
meited, s'il est trové dedenz
sache e soche, si
perderad la femme e le
seinur l'averad.
De chascuns x
hides deI hundred un
hume dedenz la feste Seint
Michel e la Seint Martin.
E si Ii guardireve averad
xxxhides, quite serrad
pur sun travail. E si
aveir trespasse par iloc
u il deivent guaiter, e ilne puissent mustrer ne cri
ne force que lur fust feite,
si rendissent l'aveir.
