<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main">Tyolet</title>
      <title type="formal">tyolet</title>
      <title type="reference">tyolet</title>
      <title type="DEAF">TyoletT</title>
      <title type="gmd">transcription électronique</title>
      <author xml:id="anonyme">anonyme</author>
      <editor>Équipe de la Base de français médiéval - ENS de Lyon / UMR 5317 IHRIM</editor>
      <funder>Laboratoire IHRIM</funder>
    </titleStmt>
    <editionStmt>
      <edition>transcripion électronique</edition>
      <respStmt xml:id="encoderDD">
        <resp>balisage du discours direct</resp>
        <name xml:id="AL">Alexei Lavrentiev</name>
      </respStmt>
      <respStmt xml:id="digitizer">
        <resp>scannage et océrisation</resp>
        <name xml:id="ES">Ekaterina Shcherbina</name>
      </respStmt>
      <respStmt xml:id="proofreader1">
        <resp>révision de tout le texte</resp>
        <name xml:id="PL">Pierre Levron</name>
      </respStmt>
      <respStmt xml:id="proofreader2">
        <resp>révision de tout le texte</resp>
        <name xml:id="VO">Vanessa Obry</name>
      </respStmt>
    </editionStmt>
    <extent>- Numéros de pages saisies : de 236 à 252 ;
            - Nombre de mots d'après la base des descripteurs : <num xml:id="nb_mots_given">4074</num> ;
            - Nombre de tokens (mots et ponctuations) d'après la base des descripteurs : <num xml:id="nb_tokens_given">4787</num>.
    </extent>
    <publicationStmt>
      <publisher><name>Projet BFM, ENS de Lyon / UMR 5317 IHRIM</name></publisher>
      <address>
        <addrLine>15, parvis René Descartes</addrLine>
        <addrLine>69342 Lyon BP7000 Cedex 07</addrLine>
        <addrLine>Tél : 04 37 37 63 10</addrLine>
        <addrLine>Mèl : bfm@ens-lyon.fr</addrLine>
        <addrLine>http://bfm.ens-lyon.fr</addrLine>
      </address>
      <idno type="bfm">http://catalog.bfm-corpus.org/tyolet</idno>
      <idno type="doi">10.34847/nkl.648457te</idno>
      <availability status="free">
        <licence target="https://www.etalab.gouv.fr/licence-ouverte-open-licence">
          <ab type="availability_bfm" subtype="libre_1b">Les Conditions générales d'utilisation de la Base de Français Médiéval (http://txm.bfm-corpus.org/bfm/files/CGU.pdf), 
            et notamment son Article 5 « PROPRIETE INTELLECTUELLE », s'appliquent à tous les textes de la BFM.</ab>
          <ab type="how_to_cite">
            <label>Comment citer ce texte :</label>
            <bibl>Tyolet, édité par Prudence Mary O'Hara Tobin, Genève, 1976. 
              Publié en ligne par la Base de français médiéval, http://catalog.bfm-corpus.org/tyolet.
              Dernière révision le 2016-06-19.</bibl>
            D'autres formules de citation de la BFM sont proposées sur la page web suivante :
            http://bfm.ens-lyon.fr/article.php3?id_article=281.</ab>
          <ab type="availability_file">
            <label>Conditions d'utilisation de ce fichier :</label>
              Ce fichier numérique est composé du corps de texte médiéval et des suppléments numériques : balisage XML-TEI, métadonnées (teiHeader), balisage du discours direct. Le texte et les suppléments numériques sont libres de droit et peuvent à ce titre être téléchargés et réutilisés librement.
            </ab>
            <ab type="availability_txmweb" subtype="illimité">
              <label>Conditions d'utilisation par le portail BFM :</label>
              Pas de limitation dans l'usage des fonctionnalités. Téléchargement autorisé.</ab>
        </licence>
      </availability>
    </publicationStmt>
    <sourceDesc>
      <biblFull>
        <titleStmt>
          <title>Les lais anonymes : le lai de Tyolet</title>
          <respStmt>
            <resp xml:id="editor">Éditeur scientifique</resp>
            <name type="person">Prudence Mary O'Hara Tobin</name>
          </respStmt>
        </titleStmt>
          <extent>17 page(s), soit 236-252
        </extent>
        <publicationStmt>
          <publisher>Droz</publisher>
          <pubPlace>Genève</pubPlace>
          <date>1976</date>
          <idno type="sigle_DEAF">TyoletT</idno>
          <idno type="cote_biblio">ENS-LSH : 84/2 A lai</idno>
        </publicationStmt><seriesStmt><title>Publications romanes et françaises</title><idno type="collection">143</idno></seriesStmt>
      </biblFull>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Projet :<name type="project">BFM - Base de français médiéval</name>
        Resp. : <name type="person">Céline Guillot-Barbance</name>
        Équipe : Base de français médiéval
        Laboratoire : UMR 5317 IHRIM
        Institution : ENS de Lyon
        <address>
          <addrLine>15, parvis René Descartes</addrLine>
          <addrLine>69342 Lyon BP7000 Cedex 07</addrLine>
          <addrLine>Tél : 04 37 37 63 10</addrLine>
          <addrLine>Mèl : bfm@ens-lyon.fr</addrLine>
          <addrLine>http://bfm.ens-lyon.fr</addrLine>
        </address>
      </p>
    </projectDesc>
    <editorialDecl>
      <p>Voir le <ref target="http://bfm.ens-lyon.fr/IMG/pdf/Manuel_Encodage_TEI.pdf">Manuel d'encodage XML-TEI de la BFM</ref>
        et les <ref target="http://bfm.ens-lyon.fr/IMG/pdf/Consignes_BFM.pdf">Consignes aux relecteurs de la BFM</ref>.</p>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="ab" occurs="5"/>
        <tagUsage gi="body" occurs="1"/>
        <tagUsage gi="div" occurs="2"/>
        <tagUsage gi="front" occurs="1"/>
        <tagUsage gi="gap" occurs="2"/>
        <tagUsage gi="lb" occurs="705"/>
        <tagUsage gi="num" occurs="35"/>
        <tagUsage gi="p" occurs="3"/>
        <tagUsage gi="pb" occurs="17"/>
        <tagUsage gi="q" occurs="42"/>
        <tagUsage gi="text" occurs="1"/>
      </namespace>
    </tagsDecl>
  </encodingDesc>
  <profileDesc>
    <creation>
      <name type="author">anonyme</name>
      <title>Tyolet</title>
      <date type="compo" when="1212-01-01" notBefore="1200-01-01" notAfter="1225-01-01" n="13">1er quart 13e s.</date>
      <date type="compo_periode">ancien</date>
      <date type="compo_sous_siecle" n="13_1">début</date>
      <date type="ms" when="1300-01-01" notBefore="1295-01-01" notAfter="1304-12-31">ca. 1300</date>
      <idno type="msIdentifier">Paris, BnF, nafr. 1104 (S)</idno>
      <region type="dialecte_auteur">Nord-Ouest</region>
    </creation>
    <langUsage>
      <language ident="fro" usage="100">Le texte est entièrement écrit en vers en ancien français.</language>
    </langUsage>
    <textDesc>
      <channel mode="w">print</channel>
      <constitution type="single"/>
      <derivation type="original"/>
      <domain type="littéraire"/>
    <factuality type="inapplicable"/>
      <interaction type="none"/>
      <preparedness type="prepared"/>
      <purpose type="narrative"/></textDesc>
    <textClass>
      <keywords scheme="http://authorities.bfm-corpus.org">
        <term type="domaine">littéraire</term>
        <term type="genre">récit bref</term>
        <term type="forme">vers</term>
        <term type="relation">original</term>
      </keywords>
    </textClass>
  </profileDesc>
  <revisionDesc>
    <change who="#ES" when="2006-10-31">scannage et océrisation</change>
    <change who="#PL" when="2006-12-31">révision de tout le texte</change>
    <change who="#VO" when="2007-03-31">révision de tout le texte</change>
    <change who="#AL" when="2016-06-19">balisage du discours direct</change>
    <change who="#AL" when="2016-06-20">balisage, numérotation et vérifications finales</change>
    <change when="2016-06-19">Dernière modification du texte</change>
    <change when="2022-11-04">Création de cet entête</change>
  </revisionDesc>
</teiHeader>
<text>
<pb n="236"/>
<front>
<div type="titre">
<p>LE LAY DE TYOLET</p>
</div>
</front>
<body>
<div>
<ab type="gv">
<lb n="0"/>C'est le lay de Tyoulet. 
<lb n="1"/>Jadis au tens q'Artur regna, 
<lb n="2"/>que il Bretaingne governa 
<lb n="3"/>que Engleterre ert apelee, 
<lb n="4"/>dont n'estoit mie si puplee
<pb n="237"/>
<lb n="5"/>conme ele or e, ce m'est a vis ; 
<lb n="6"/>mes Artur, qui ert de grant pris, 
<lb n="7"/>avoit o lui tex chevaliers 
<lb n="8"/>qui molt erent hardiz e fiers. 
<lb n="9"/>Encor en i a il assez 
<lb n="10"/>qui molt sont preuz e alosez, 
<lb n="11"/>mes ne sont pas de la maniere 
<lb n="12"/>qu'il estoient du tens ariere, 
<lb n="13"/>que li chevalier plus poissant, 
<lb n="14"/>li miedre, li plus despendant, 
<lb n="15"/>soloient molt par nuit errer, 
<lb n="16"/>aventures querre e trover, 
<lb n="17"/>e par jor ensement erroient, 
<lb n="18"/>que il escuier nen avoient, 
<lb n="19"/>si erroient si toutejor. 
<lb n="20"/>Ne trouvassent meson ne tor 
<lb n="21"/>ou <num>.II.</num> ou <num>.III.</num> par aventure, 
<lb n="22"/>e ensement par nuit oscure 
<lb n="23"/>aventures beles trovoient 
<lb n="24"/>qu'il disoient e racontoient. 
<lb n="25"/>A la cort erent racontees, 
<lb n="26"/>si conme eles erent trovees ; 
<lb n="27"/>Li preude clerc qui donc estoient 
<lb n="28"/>totes escrire les fesoient ; 
<lb n="29"/>mises estoient en latin 
<lb n="30"/>e en escrit em parchemin, 
<lb n="31"/>por ce qu'encor tel tens seroit 
<lb n="32"/>que l'en volentiers les orroit. 
<lb n="33"/>Or sont dites e racontees, 
<lb n="34"/>de latin en romanz trovees ; 
<lb n="35"/>Bretons en firent lais plusors, 
<lb n="36"/>si con dïent nos ancessors. 
<lb n="37"/><num>.I.</num> en firent que vos dirai, 
<lb n="38"/>selonc le conte que je sai 
<lb n="39"/>du vallet bel e engingnos, 
<lb n="40"/>hardi e fier e coragos. 
<lb n="41"/>Tyolet estoit apelez, 
<lb n="42"/>de bestes prendre sot assez 
<lb n="43"/>que par son sisflé les prenoit, 
<lb n="44"/>totes les bestes qu'il voloit. 
<lb n="45"/>Une fee ce li ora 
<lb n="46"/>e a sifler li enseigna ; 
<lb n="47"/>Dex onc nule beste ne fist 
<lb n="48"/>qu'il a son siflé ne preïst.
<lb n="49"/>Une dame sa mere estoit 
<lb n="50"/>qui en <num>.I.</num> bois adés manoit,
<pb n="238"/>
<lb n="51"/><num>.I.</num> chevalier ot a seignor
<lb n="52"/>qui mest ilec e nuit e jor ;
<lb n="53"/>tot seul en la forest manoit,
<lb n="54"/>de dis liues meson n'avoit.
<lb n="55"/>Mort est, bien ot passé <num>.XV.</num> anz,
<lb n="56"/>e Tyolet fu biaus e granz,
<lb n="57"/>mes onques chevalier armé
<lb n="58"/>n'ot veü en tot son aé,
<lb n="59"/>ne autres genz gueres sovent
<lb n="60"/>n'ot il pas veu ensement.
<lb n="61"/>El bois o sa mere manoit,
<lb n="62"/>onques jor fors issu n'avoit,
<lb n="63"/>en la forez ot sejorné,
<lb n="64"/>car sa mere l'ot molt amé.
<lb n="65"/>Dont i ala qant li plesoit,
<lb n="66"/>nul autre mestier ne faisoit.
<lb n="67"/>Qant les bestes sifler l'ooient,
<lb n="68"/>tot erramment a li venoient ;
<lb n="69"/>de ceus que il voloit, tuoit
<lb n="70"/>e a sa mere les portoit.
<lb n="71"/>De ce vivoit lui e sa mere,
<lb n="72"/>e il n'avoit ne suer ne frere ;
<lb n="73"/>la dame molt vaillanz estoit,
<lb n="74"/>e leaument se contenoit.
<lb n="75"/>A son filz <num>.I.</num> jor demanda
<lb n="76"/>bonement, car forment l'ama,
<lb n="77"/>el bois alast, <num>.I.</num> cerf preïst,
<lb n="78"/>e il son conmandement fist.
<lb n="79"/>El bois hastivement ala
<lb n="80"/>si con sa mere conmanda.
<lb n="81"/>Desq'a tierce a el bois alé,
<lb n="82"/>beste ne cerf n'i a trouvé.
<lb n="83"/>A soi molt corrouciez estoit
<lb n="84"/>de ce que beste ne trouvoit ;
<lb n="85"/>droit vers meson s'en volt aler,
<lb n="86"/>qant soz <num>.I.</num> arbre vit ester
<lb n="87"/><num>.I.</num> cerf qui ert e grant e gras,
<lb n="88"/>e il sifla eneslepas.
<lb n="89"/>Li cers l'oï, si regarda,
<lb n="90"/>ne l'atendi, ainz s'en ala ;
<lb n="91"/>le petit pas du bois issi, 
<lb n="92"/>e Tyolet tant le sevi
<lb n="93"/>q'a une eve l'a droit mené ;
<lb n="94"/>le cerf s'en est outre passé.
<lb n="95"/>L'eve estoit grant e ravineuse
<pb n="239"/>
<lb n="96"/>e lee e longue e perilleuse.
<lb n="97"/>Li cers outre l'eve passa,
<lb n="98"/>e Tyolet se regarda
<lb n="99"/>tries soi, si vit venir errant
<lb n="100"/><num>.I.</num> chevrel cras e lonc e grant.
<lb n="101"/>Arestut soi e si sifla,
<lb n="102"/>e li chevreus vers lui ala ;
<lb n="103"/>sa main tendi, illec l'ocist,
<lb n="104"/>son costel tret, el cors li mist.
<lb n="105"/>Endementres qu'il l'escorcha,
<lb n="106"/>e li cers se tranfigura
<lb n="107"/>qui outre l'eve s'estoit mis,
<lb n="108"/><gap rend="points"/>
<lb n="109"/>e <num>.I.</num> chevalier resembloit 
<lb n="110"/>tot armé sor l'eve s'estoit, 
<lb n="111"/>sor <num>.I.</num> cheval detries comé, 
<lb n="112"/>s'estoit com chevalier armé. 
<lb n="113"/>Le vallet l'a aparceü, 
<lb n="114"/>onques mes tel n'avoit veü ; 
<lb n="115"/>a merveilles l'a esgardé 
<lb n="116"/>e longuement l'a avisé ; 
<lb n="117"/>de tel chose se merveilloit 
<lb n="118"/>car onques mes veu n'avoit. 
<lb n="119"/>Ententivement l'avisa ; 
<lb n="120"/>le chevalier l'aresonna, 
<lb n="121"/>a lui parla premierement, 
<lb n="122"/>molt bel e aimablement ; 
<lb n="123"/>demande li qui il estoit, 
<lb n="124"/>q'aloit querant, quel non avoit. 
<lb n="125"/>E Tyolet li respondi, 
<lb n="126"/>qui molt estoit preuz e hardi, 
<lb n="127"/>filz a la veve dame estoit 
<lb n="128"/>qui en la grant forez manoit,
<lb n="129"/><q>« e Tyolet m'apele l'on,
<lb n="130"/>cil qui nomer veulent mon non. 
<lb n="131"/>Or me dites, se vos savez, 
<lb n="132"/>qui vos estes, quel non avez. »</q> 
<lb n="133"/>E cil li respondi errant 
<lb n="134"/>qui seur la rive fu estant, 
<lb n="135"/>que chevalier ert apelé. 
<lb n="136"/>E Tyolet a demandé 
<lb n="137"/>quel beste chevalier estoit, 
<lb n="138"/>ou conversoit e dont venoit.
<lb n="139"/><q>« Par foi, fet il, jel te dirai, 
<lb n="140"/>que ja mot ne t'en mentirai. 
<lb n="141"/>C'est une beste molt cremue, 
<lb n="142"/>autres bestes prent e menjue, 
<lb n="143"/>el bois converse molt souvent,
<pb n="240"/>
<lb n="144"/>e a plainne terre ensement.</q>
<lb n="145"/><q>- Par foi, fet il, merveilles oi. 
<lb n="146"/>Car onques, puis que aler soi
<lb n="147"/>e que par bois pris a aler, 
<lb n="148"/>ainz tel beste ne poi trover. 
<lb n="149"/>Si connois je ors e lions, 
<lb n="150"/>e totes autres venoisons ; 
<lb n="151"/>n'a beste el bois que ne connoisse, 
<lb n="152"/>e que ne preigne sanz angoisse, 
<lb n="153"/>ne mes vos que ne connois mie. 
<lb n="154"/>Molt resemblez beste hardie. 
<lb n="155"/>Or me dites, chevalier beste, 
<lb n="156"/>que est ice sor vostre teste ? 
<lb n="157"/>E qu'est ice q'au col vos pent ? 
<lb n="158"/>roge est e si reluist forment.</q>
<lb n="159"/><q>- Par foi, fet il, jel te dirai, 
<lb n="160"/>que ja de mot n'en mentirai. 
<lb n="161"/>C'est une coiffe, hiaume a non, 
<lb n="162"/>si est d'acier tout environ,
<lb n="163"/>e cest mantel q'ai afublé, 
<lb n="164"/>c'est <num>.I.</num> escu a or bendé.</q>
<lb n="165"/><q>- E qu'est ice q'avez vestuz, 
<lb n="166"/>qui si est pertuisiez menuz ?</q>
<lb n="167"/><q>- Une cote est, de fer ovree ; 
<lb n="168"/>hauberc est par non apelee.</q>
<lb n="169"/><q>- E qu'est ice q'avez chaucié ? 
<lb n="170"/>Dites le moi par amistié.</q>
<lb n="171"/><q>- Chauces de fer sont apelees ; 
<lb n="172"/>bien sont fetes e bien ovrees.</q>
<lb n="173"/><q>- E ce que est que ceint avez ? 
<lb n="174"/>Dites le moi se vos volez.</q>
<lb n="175"/><q>- Espee a non, molt par est bele, 
<lb n="176"/>trenchant e dure la lemele.</q>
<q><lb n="177"/>- Ice lonc fust que vos portez ? 
<lb n="178"/>Dites le moi, ne me celez.</q>
<lb n="179"/><q>- Veus le savoir ?</q> <q>- Oïl, par foi.</q>
<lb n="180"/><q>- Une lance que port o moi. 
<lb n="181"/>Or t'en ai dit la verité
<lb n="182"/>de qanque tu m'as demandé.</q>
<lb n="183"/><q>- Sire, fet il, vostre merci. 
<lb n="184"/>Car pleüst Dieu qui ne menti, 
<lb n="185"/>que j'eusse tiex garnemenz 
<lb n="186"/>con vos avez, si biaus, si genz, 
<lb n="187"/>tel cote eüsse, e tel mantel 
<lb n="188"/>con vos avez, e tel chapel.
<lb n="189"/>Or me dites, chevalier beste, 
<lb n="190"/>por Deu, e por la seue feste, 
<lb n="191"/>se il est auques de tiex bestes
<pb n="241"/>
<lb n="192"/>ne de si beles con vos estes.
<lb n="193"/>- Oïl, fet il, veraiement,
<lb n="194"/>ja, t'en mosterré plus de cent. »</q> 
<lb n="195"/>Ne demora que un petit, 
<lb n="196"/>si conme li contes nos dit, 
<lb n="197"/>que <num>.II.</num> cenz chevaliers armez 
<lb n="198"/>erroient tres par mi uns prez, 
<lb n="199"/>qui de la cort au roi venoient ; 
<lb n="200"/>son conmandement fet avoient. 
<lb n="201"/>Une fort meson orent prise 
<lb n="202"/>e en feu e en charbon mise, 
<lb n="203"/>si s'en repairent tuit armé, 
<lb n="204"/>en <num>.III.</num> eschieles bien serré. 
<lb n="205"/>Chevalier beste dont parla 
<lb n="206"/>a Tyolet, e conmanda 
<lb n="207"/>c'un seul petit avant alast, 
<lb n="208"/>outre la riviere gardast. 
<lb n="209"/>Cil a fet son conmandement, 
<lb n="210"/>outre regarde isnelement, 
<lb n="211"/>si voit errer les chevaliers 
<lb n="212"/>trestot armez sor les destriers.
<lb n="213"/><q>« Par foi, fet il, or voi les bestes 
<lb n="214"/>qui totes ont coiffes es testes. 
<lb n="215"/>Onques mes tex bestes ne vi, 
<lb n="216"/>ne tiex coiffes con je voi ci.
<lb n="217"/>Car pleüst or Dieu a sa feste 
<lb n="218"/>que je fusse chevalier beste ! »</q> 
<lb n="219"/>Cil ra donques a lui parlé 
<lb n="220"/>qui sor la rive estoit armé :
<lb n="221"/><q>« Seroies tu preuz e hardi ?</q>
<lb n="222"/><q>- Oïl, par foi, le vos afi. »</q> 
<lb n="223"/>Si li a dit : <q>« Or t'en iras, 
<lb n="224"/>e qant ta mere reverras
<lb n="225"/>e ele parlera a toi, »
<lb n="226"/>ele dira : <q>“ Biaus filz, di moi
<lb n="227"/>de quoi tu penses, e que as ? ”</q>
<lb n="228"/>E tu li diz eneslepas
<lb n="229"/>que tu as assez a penser,
<lb n="230"/>que tu vorroies resembler
<lb n="231"/>chevalier beste que veïs,
<lb n="232"/>e por ce eres tu pensis.
<lb n="233"/>E ele te dira briement
<lb n="234"/>que ce li poise molt forment
<lb n="235"/>que tu as tel beste veüe,
<lb n="236"/>que autre engingne e autre tue.
<lb n="237"/>E tu li dis que par ta foi,
<pb n="242"/>
<lb n="238"/>que male joie avra de toi 
<lb n="239"/>si tu ne puez estre tel beste, 
<lb n="240"/>e tel coiffe avoir en ta teste ; 
<lb n="241"/>e des ce qu'ele ce orra, 
<lb n="242"/>isnelement t'aportera 
<lb n="243"/>toute autretele vesteüre, 
<lb n="244"/>cote e mantel, coiffe e ceinture, 
<lb n="245"/>e chauces e lonc fust plané, 
<lb n="246"/>tex con tu as ci esgardé. »</q>
<lb n="247"/>Atant Tyolet s'en depart, 
<lb n="248"/>qu'en meson soit molt li est tart. 
<lb n="249"/>Puis a a sa mere donné 
<lb n="250"/>le chevrel qu'il ot aporté, 
<lb n="251"/>e s'aventure li conta 
<lb n="252"/>tot ainsi conme il la trova. 
<lb n="253"/>Sa mere li respont briement 
<lb n="254"/>que ce li poise molt forment
<lb n="255"/><q>« que tu as tel beste veüe
<lb n="256"/>qui mainte autre prent e manjue.</q>
<lb n="257"/><q>- Par foi, fet il, or est ainsi : 
<lb n="258"/>si je tel beste con je vi
<lb n="259"/>ne puis estre, bien sai e voi 
<lb n="260"/>que male joie avrez de moi. »</q> 
<lb n="261"/>Mes sa mere, qant ce oï, 
<lb n="262"/>isnelement li respondi ; 
<lb n="263"/>totes les armes que ele a 
<lb n="264"/>isnelement li aporta, 
<lb n="265"/>qui son seignor orent esté : 
<lb n="266"/>molt en a bien son filz armé. 
<lb n="267"/>E qant el cheval fu monté, 
<lb n="268"/>chevalier beste a bien semblé.
<lb n="269"/><q>« Sez or, biaus filz, que tu feras ? 
<lb n="270"/>Tot droit au roi Artur iras,
<lb n="271"/>e de ce te dirai la somme :
<lb n="272"/>ne t'aconpaingnes a nul homme
<lb n="273"/>ne a fame ne donoier
<lb n="274"/>qui commune soit de mestier. »</q>
<lb n="275"/>Atant s'en est de li torné,
<lb n="276"/>el l'a baisié e acolé.
<lb n="277"/>Tant a erré par ses jornees
<lb n="278"/>que monz que terres que valees,
<lb n="279"/>q'a la cort le roi est venu,
<lb n="280"/>qui cortois rois e vaillanz fu.
<pb n="243"/>
<lb n="281"/>Li rois a son mengier seoit, 
<lb n="282"/>servir richement se fesoit, 
<lb n="283"/>e Tyolet est enz entrez 
<lb n="284"/>si conme il vint trestot armez. 
<lb n="285"/>A cheval vint devant le dois 
<lb n="286"/>la ou seoit Artur le rois. 
<lb n="287"/>Onques <num>.I.</num> mot ne li sonna, 
<lb n="288"/>ne noient ne l'aresonna.
<lb n="289"/><q>« Amis, fet li rois, descendez, 
<lb n="290"/>e avec nos mengier venez,
<lb n="291"/>si me dites que vos querez, 
<lb n="292"/>qui vos estes, quel non avez.</q>
<lb n="293"/><q>- Par foi, fet il, jel vos dirai, 
<lb n="294"/>que ja ançois ne mengerai. 
<lb n="295"/>Rois, j'ai a non chevalier beste, 
<lb n="296"/>a mainte en ai trenchié la teste, 
<lb n="297"/>e Tyolet m'apele l'on.
<lb n="298"/>Molt sai bien prendre venoison. 
<lb n="299"/>Filz sui, biau sire, s'il vos plest, 
<lb n="300"/>a la veve de la forest ; 
<lb n="301"/>a vos m'envoie certement 
<lb n="302"/>tot por aprendre afetement. 
<lb n="303"/>Sens voil aprendre e cortoisie, 
<lb n="304"/>savoir voil de chevalerie, 
<lb n="305"/>a tornoier e a joster, 
<lb n="306"/>a despendrë, e a donner. 
<lb n="307"/>Car ainz ne fu ja cort de roi, 
<lb n="308"/>ne jamés n'iert si con je croi, 
<lb n="309"/>ou tant ait bien n'afetement, 
<lb n="310"/>cortoisie, n'ensaingnement. 
<lb n="311"/>Or, vos ai dit ce que j'ai quis, 
<lb n="312"/>rois, or me dites vostre avis. »</q> 
<lb n="313"/>Li rois li dit : <q>« Dan chevalier, 
<lb n="314"/>je vos retien, venez mengier.</q>
<lb n="315"/><q>- Sire, fet il, vostre merci. »</q> 
<lb n="316"/>Tyolet donques descendi,
<lb n="317"/>de ses armes s'est desarmé, 
<lb n="318"/>si s'est vestu e afublé 
<lb n="319"/>de cote e de mantel legier, 
<lb n="320"/>ses mains leve, si va mengier. 
<lb n="321"/>Atant es vos une pucele, 
<lb n="322"/>une orgueilleuse damoisele.
<pb n="244"/>
<lb n="323"/>De sa biauté ne voil parler.
<lb n="324"/><gap rend="points"/>
<lb n="325"/>Onques Dido, ce m'est a vis, 
<lb n="326"/>ne Elainne n'ot si cler vis. 
<lb n="327"/>Fille au roi de Logres estoit, 
<lb n="328"/>sor <num>.I.</num> blanc palefroi seoit, 
<lb n="329"/><num>.I.</num> blanc brachet tries soi portoit ; 
<lb n="330"/>une sonnete d'or avoit 
<lb n="331"/>pendue au col du blanc brachet. 
<lb n="332"/>Molt ot le poil deugié e net. 
<lb n="333"/>Tot a cheval en est venue 
<lb n="334"/>devant le roi, si le salue.
<lb n="335"/><q>« Rois Artur, sire, Dex te saut, 
<lb n="336"/>le tot poissant qui maint en haut.
<lb n="337"/>- Bele amie, celui vos gart 
<lb n="338"/>qui les bons retient a sa part.</q>
<lb n="339"/><q>- Sire, je sui une meschine, 
<lb n="340"/>fille de roi e de roïne, 
<lb n="341"/>e de Logres est rois mon pere, 
<lb n="342"/>n'ont plus enfanz, li ne ma mere. 
<lb n="343"/>E si vos mandent par amor, 
<lb n="344"/>conmë a roi de grant valor, 
<lb n="345"/>s'il i a de vos chevaliers 
<lb n="346"/>nul qui tant soit hardiz ne fiers, 
<lb n="347"/>qui le blanc pié du cerf tranchast. 
<lb n="348"/>Biau sire, celui me donnast ; 
<lb n="349"/>icelui a seignor prendroie, 
<lb n="350"/>de nul autre cure n'avroie. 
<lb n="351"/>Ja nus hon n'avra m'amistié, 
<lb n="352"/>s'il ne me donne le blanc pié 
<lb n="353"/>du cerf qui est e bel e grant, 
<lb n="354"/>e qui tant a le poil luisant 
<lb n="355"/>por poi qu'il ne semble doré ; 
<lb n="356"/>de <num>.VII.</num> lïons est bien gardé.</q>
<lb n="357"/><q>- Par foi, fet il rois, vos creant 
<lb n="358"/>que iltel soit le covenant
<lb n="359"/>que cil a fame vos avra
<lb n="360"/>qui le pié du cerf vos donra.</q>
<lb n="361"/><q>- E je, dan rois, si le creant 
<lb n="362"/>que iltel soit le covenant. »</q> 
<lb n="363"/>Tel covenant ont afermé
<lb n="364"/>e entr'eus <num>.II.</num> bien devisé. 
<lb n="365"/>En la sale n'ot chevalier 
<lb n="366"/>qui de rien feïst a prisier, 
<lb n="367"/>qui ne deïst que il iroit
<pb n="245"/>
<lb n="368"/>quere le cerf, s'il le savoit. 
<lb n="369"/><q>« Cest brachet, dist el, vos menra 
<lb n="370"/>la ou le cerf converse e va. »</q> 
<lb n="371"/>Lodoër molt le covoita, 
<lb n="372"/>le cerf querre premiers ala. 
<lb n="373"/>Au roi Artu l'a demandé 
<lb n="374"/>e il ne li a pas veé. 
<lb n="375"/>Le brachet prent, si est montez, 
<lb n="376"/>le pié du cerf est querre alez. 
<lb n="377"/>Le brachet qui o lui ala, 
<lb n="378"/>droit a une eve le mena, 
<lb n="379"/>qui molt estoit e grant e lee 
<lb n="380"/>e noire e hisdeuse e enflee, 
<lb n="381"/>qatre <num>.C.</num> toises ot de lé 
<lb n="382"/>e bien <num>.C.</num> de parfondee. 
<lb n="383"/>E le brachet en l'eve entra ; 
<lb n="384"/>selonc son sens tres bien cuida , 
<lb n="385"/>que Lodoër enz se meïst, 
<lb n="386"/>mais de tot ce noient ne fist. 
<lb n="387"/>Il dit que il n'i enterra, 
<lb n="388"/>car de morir nul talent n'a ; 
<lb n="389"/>a soi redit a chief de pose : 
<lb n="390"/><q>« qui soi nen a, n'a nule chose. 
<lb n="391"/>Bon chastel garde, ce m'est vis, 
<lb n="392"/>qui garde qu'il ne soit maumis. »</q> 
<lb n="393"/>Dont s'en est li brachez issuz, 
<lb n="394"/>a Lodoër est revenuz, 
<lb n="395"/>e Lodoër si s'en ala 
<lb n="396"/>e le brachet tries soi porta. 
<lb n="397"/>Droit a la cort en vint errant, 
<lb n="398"/>ou li barnages estoit grant, 
<lb n="399"/>le brachet rent a la pucele, 
<lb n="400"/>qui molt estoit cortoise e bele. 
<lb n="401"/>Dont li a li rois demandé 
<lb n="402"/>s'il avoit le pié aporté, 
<lb n="403"/>e Lodoër li respondi 
<lb n="404"/>qu'encor en ert autre escharni. 
<lb n="405"/>Dont l'ont par la sale gabé, 
<lb n="406"/>e il lor a le chief crollé, 
<lb n="407"/>si lor a dit que il alassent 
<lb n="408"/>quere le pié, si l'aportassent. 
<lb n="409"/>Quere le cerf molt i alerent 
<lb n="410"/>e la pucele demanderent. 
<lb n="411"/>N'en i ot nul qui la alast 
<lb n="412"/>q'autretel chançon ne chantast 
<lb n="413"/>con Lodïer chanté avoit, 
<lb n="414"/>qui vaillanz chevaliers estoit, 
<lb n="415"/>fors seulement <num>.I.</num> chevalier
<pb n="246"/>
<lb n="416"/>qui molt estoit preuz e legier ;
<lb n="417"/>chevalier beste ert apelé,
<lb n="418"/>e Tyolet estoit nommé.
<lb n="419"/>Cil s'en est droit au roi alé,
<lb n="420"/>hastivement a demandé
<lb n="421"/>que cele gardee li soit,
<lb n="422"/>que le pié blanc querrë iroit.
<lb n="423"/>Jamés, ce dit, ne revendra
<lb n="424"/>devant ice que ill avra
<lb n="425"/>le pié blanc destre au cerf trenchié.
<lb n="426"/>Li rois li a donné congié,
<lb n="427"/>e Tyolet s'est adoubé
<lb n="428"/>e de ses armes bien armé.
<lb n="429"/>A la pucele dont ala,
<lb n="430"/>son blanc brachet requis li a.
<lb n="431"/>El li a bonement baillié,
<lb n="432"/>e il a pris de li congié.
<lb n="433"/>Tant ont chevauchié e erré
<lb n="434"/>que andui sont venu au gué,
<lb n="435"/>a la grant eve ravineuse
<lb n="436"/>qui molt ert parfonde e hisdeuse.
<lb n="437"/>Le brachet s'est en l'eve mis,
<lb n="438"/>outre s'en vet, noant totdis ;
<lb n="439"/>aprés lui se met Tyolet,
<lb n="440"/>tant a suï le blanc brachet
<lb n="441"/>sor son destrier sor coi il sist
<lb n="442"/>que a la terre fors s'en ist.
<lb n="443"/>Dont l'a le brachet tant mené
<lb n="444"/>que il li a le cerf moustré.
<lb n="445"/><num>.VII.</num> granz lïons le cerf gardoient
<lb n="446"/>e de molt grand amor l'amoient.
<lb n="447"/>E Tyolet garde, sel voit
<lb n="448"/>enmi <num>.I.</num> pré ou il paissoit,
<lb n="449"/>n'i avoit nul des <num>.VII.</num> lïons.
<lb n="450"/>Tyolet fiert des esperons, 
<lb n="451"/>devant le cerf le fet aler.
<lb n="452"/>Tyolet prent lors a sifler,
<lb n="453"/>e li cers molt beninement
<lb n="454"/>vers Tyolet vient erramment.
<lb n="455"/>E Tyolet <num>.II.</num> fois sifla,
<lb n="456"/>li cerf du tot donc s'aresta.
<lb n="457"/>S'espee tret isnelement,
<lb n="458"/>du cerf le blanc pié destre prent,
<lb n="459"/>par mi la jointe li trencha,
<lb n="460"/>dedenz sa huese le bouta.
<lb n="461"/>Le cerf cria molt hautement,
<lb n="462"/>e li lïon tout erroment
<lb n="463"/>grant aleüre i sont venu,
<pb n="247"/>
<lb n="464"/>Tyolet ont aparceü. 
<lb n="465"/>Uns des lïons a si navré 
<lb n="466"/>le cheval ou il sist armé 
<lb n="467"/>que la destre espaule devant 
<lb n="468"/>e cuir e char en va portant. 
<lb n="469"/>Qant Tyolet a ce veü, 
<lb n="470"/><num>.I.</num> des lions a si feru 
<lb n="471"/>de l'espee que il porta, 
<lb n="472"/>que les ners du piz li trencha : 
<lb n="473"/>de ce lion n'ot il plus guerre. 
<lb n="474"/>Son cheval chiet soz lui a terre, 
<lb n="475"/>donques Tyolet le guerpi 
<lb n="476"/>e li lïon l'ont assailli. 
<lb n="477"/>De totes parz assailli l'ont 
<lb n="478"/>son bon hauberc rompu li ont, 
<lb n="479"/>la char des braz e des costez 
<lb n="480"/>en plusors leus est si navrez 
<lb n="481"/>a poi que il nel devoroient ; 
<lb n="482"/>tote la char li desciroient, 
<lb n="483"/>mes il les a trestoz tuez ; 
<lb n="484"/>a poi ne s'en est delivrez. 
<lb n="485"/>Dejoste les lïons chaï 
<lb n="486"/>qui malement l'orent bailli, 
<lb n="487"/>e de son cors si domagié ; 
<lb n="488"/>ja par li n'ert mes redrecié. 
<lb n="489"/>Es vos errant <num>.I.</num> chevalier 
<lb n="490"/>e sist sor <num>.I.</num> ferrant destrier. 
<lb n="491"/>Arestut soi, si resgarda, 
<lb n="492"/>molt par le plaint e regreta. 
<lb n="493"/>E Tyolet les eulz ouvri, 
<lb n="494"/>qui du travail ert endormi, 
<lb n="495"/>s'aventure li a contee, 
<lb n="496"/>e de chief en chief racontee ; 
<lb n="497"/>de sa huese le pié sacha, 
<lb n="498"/>e au chevalier le bailla. 
<lb n="499"/>E cil l'en a molt mercïé 
<lb n="500"/>car le pié a forment amé, 
<lb n="501"/>de lui prent congié, si s'en va. 
<lb n="502"/>En la voie se porpensa 
<lb n="503"/>que se le chevalier vivoit 
<lb n="504"/>qui le pié donné li avoit 
<lb n="505"/>se il ne s'en voloit fuir, 
<lb n="506"/>que mal l'em porroit avenir. 
<lb n="507"/>Ariere torne maintenant. 
<lb n="508"/>En pensé a, e en talent 
<lb n="509"/>que le chevalier ocirra, 
<lb n="510"/>jamés ne li chalangera. 
<lb n="511"/>Par mi le cors bien l'asena
<pb n="248"/>
<lb n="512"/>- de cele plaie bien garra -
<lb n="513"/>bien le cuida avoir ocis,
<lb n="514"/>atant s'est a la voie mis.
<lb n="515"/>Tant a son droit chemin tenu
<lb n="516"/>Q'a la cort le roi est venu.
<lb n="517"/>La pucele au roi demanda,
<lb n="518"/>le blanc pié du cerf li mostra ;
<lb n="519"/>mes il n'ot pas le blanc brachet
<lb n="520"/>qui au cerf conduit Tyolet :
<lb n="521"/>bien le garda e main e soir ;
<lb n="522"/>mes de ce ne puet il chaloir.
<lb n="523"/>Cil qui le pié ot aporté,
<lb n="524"/>qui que l'eüst au cerf coupé
<lb n="525"/>par covenant velt la pucele
<lb n="526"/>qui tant par est e noble e bele.
<lb n="527"/>Mes li rois qui tant sages fu,
<lb n="528"/>por Tyolet qui n'ert venu,
<lb n="529"/>respit d'uit jors li demanda ;
<lb n="530"/>adonc sa cort assemblera.
<lb n="531"/>N'i avoit or fors sa mesniee
<lb n="532"/>qui molt ert franche e enseingniee.
<lb n="533"/>Dont a cil le respit donné
<lb n="534"/>e en la cort tant sejorné.
<lb n="535"/>Mes Gauvains, qui tant fu cortois
<lb n="536"/>e bien apris en toutes lois,
<lb n="537"/>est alé querre Tyolet,
<lb n="538"/>car repairié fu le brachet,
<lb n="539"/>e il l'a avec lui mené.
<lb n="540"/>Tost le brachet l'a amené
<lb n="541"/>qu'il l'a trové en pasmoisons
<lb n="542"/>el pré dejoste les lïons.
<lb n="543"/>Qant Gauvains le chevalier voit 
<lb n="544"/>e l'ocise que fet avoit, 
<lb n="545"/>molt plaint le vaillant chevalier. 
<lb n="546"/>Semprés descent de son destrier, 
<lb n="547"/>molt doucement l'aresonna. 
<lb n="548"/>Tyolet foiblement parla
<lb n="549"/> e, neporqant, de s'aventure 
<lb n="550"/>li a conté toute la pure. 
<lb n="551"/>Atant es vos une pucele 
<lb n="552"/>sor une mule gente e bele. 
<lb n="553"/>Gauvain gentement salua, 
<lb n="554"/>e Gauvains bien rendu li a, 
<lb n="555"/>e puis l'a a soi apelee,
<pb n="249"/>
<lb n="556"/>estroitement l'a acolee,
<lb n="557"/>si li prie molt doucement
<lb n="558"/>e molt tres amiablement
<lb n="559"/>qu'ele portast cel chevalier
<lb n="560"/>qui molt par fesoit a proisíer,
<lb n="561"/>a la noire montaingne au miere.
<lb n="562"/>E cele a fete sa proiere ;
<lb n="563"/>le chevalier en a porté
<lb n="564"/>e au mire l'a conmandé.
<lb n="565"/>De par Gauvain li conmanda,
<lb n="566"/>cil volentiers receü l'a.
<lb n="567"/>De ses armes l'a despoillié,
<lb n="568"/>sor une table l'a couchié,
<lb n="569"/>e ses plaies li a lavees
<lb n="570"/>qui molt erent ensanglentees.
<lb n="571"/>Qant il l'a par trestout curé,
<lb n="572"/>le sanc fegié d'entor osté,
<lb n="573"/>bien a veu que il garroit,
<lb n="574"/>au chief d'un mois tot sain seroit.
<lb n="575"/>Entretant fu Gauvains venu
<lb n="576"/>e en la sale descendu.
<lb n="577"/>Le chevalier i a trouvé
<lb n="578"/>qui le blanc pié ot aporté.
<lb n="579"/>Tant s'est en la cort demorez
<lb n="580"/>que les vuit jors sont trespassez.
<lb n="581"/>Dont vint au roi, su salua,
<lb n="582"/>son covenant li demanda
<lb n="583"/>que la pucele ot devisé
<lb n="584"/>e il endroit soi creanté,
<lb n="585"/>qui que le blanc pié li donroit
<lb n="586"/>que ele a seignor le prendroit.
<lb n="587"/>Li rois dist : <q>« Ce est verité. »</q>
<lb n="588"/>Qant Gauvains ot tot escouté,
<lb n="589"/>eneslepas avant sailli,
<lb n="590"/>e dist au roi : <q>« N'est pas ainsi.
<lb n="591"/>Se por ce non que je ne doi
<lb n="592"/>ci, devant vos qui estes roi,
<lb n="593"/>desmentir onques chevalier,
<lb n="594"/>serjant, garçon ne escuier,
<lb n="595"/>je deïsse qu'il mespreïst ;
<lb n="596"/>n'onques du cerf le pié ne prist
<lb n="597"/>en la maniere que il conte.
<lb n="598"/>Molt fet au chevalier grant honte
<lb n="599"/>qui d'autrui fet se velt loer
<lb n="600"/>e autrui mantel afubler
<lb n="601"/>e d'autrui bouzon velt bien trere
<lb n="602"/>e loer soi d'autrui afere
<lb n="603"/>e par autrui main velt joster
<pb n="250"/>
<lb n="604"/>e hors du buisson velt trainer 
<lb n="605"/>le serpent qui tant est cremu. 
<lb n="606"/>Or, si n'i sera ja veü 
<lb n="607"/>ce que vos dites rien ne vaut. 
<lb n="608"/>Aillors ferez vostrë assaut, 
<lb n="609"/>aillors porchacier vos irez, 
<lb n="610"/>la pucele n'emporterez.</q>
<lb n="611"/><q>- Par foi, fet il, Sire Gauvain, 
<lb n="612"/>or me tenez vos por vilain
<lb n="613"/>qui me dites que n'os porter
<lb n="614"/>ma lance en estor por joster,
<lb n="615"/>bien sai trere d'autrui bouzon
<lb n="616"/>e par autrui main du buisson
<lb n="617"/>le serpent trere q'avez dit ?
<lb n="618"/>N'est nul, si con je croi e cuit,
<lb n="619"/>se vers moi le voloit prover
<lb n="620"/>qu'en champ ne m'en peüst trover. »</q>
<lb n="621"/>En ce qu'en cel estrif estoient, 
<lb n="622"/>par la sale gardent, si voient 
<lb n="623"/>Tyolet, qui estoit venu 
<lb n="624"/>e hors au perron descendu. 
<lb n="625"/>Li rois contre lui s'est levez, 
<lb n="626"/>ses braz li a au col getez, 
<lb n="627"/>puis le baise par grant amor. 
<lb n="628"/>Cil l'encline conme a seignor. 
<lb n="629"/>Gauvains le baise, e Uriain, 
<lb n="630"/>Keu, e Evain, le filz Morgain, 
<lb n="631"/>e Lodoier l'ala besier 
<lb n="632"/>e tuit li autre chevalier. 
<lb n="633"/>Li chevaliers, qant il ce voit, 
<lb n="634"/>qui la pucele avoir voloit 
<lb n="635"/>par le pié qu'il ot aporté 
<lb n="636"/>que Tyolet li ot donné, 
<lb n="637"/>au roi Artur dont reparla 
<lb n="638"/>e sa requeste demanda. 
<lb n="639"/>Mes Tyolet, qant il ce sot 
<lb n="640"/>que la pucele demandot, 
<lb n="641"/>a lui parla molt doucement, 
<lb n="642"/>e li demanda benement :
<lb n="643"/><q>« Dan chevaliers, dites le moi, 
<lb n="644"/>tant conme estes devant le roi, 
<lb n="645"/>par quel reson volez avoir
<lb n="646"/>la pucele, je voil savoir.</q>
<pb n="251"/>
<lb n="647"/><q>- Par foi, fet il, je vos dirai : 
<lb n="648"/>por ce que aporté li ai
<lb n="649"/>le blanc pié du cerf sejorné ; 
<lb n="650"/>li rois e li l'ont creanté.</q>
<lb n="651"/><q>- Trenchastes vos au cerf le pié ? 
<lb n="652"/>Se ce est voir, ne soit noié.
<lb n="653"/>- Ouïl, fet il, je l'i trenchai 
<lb n="654"/>e ici o moi l'aportai.</q>
<lb n="655"/><q>- E les <num>.VII.</num> lïons qui ocist ? »</q> 
<lb n="656"/>Cil l'esgarda, nul mot ne dit, 
<lb n="657"/>ainz rogi molt e eschaufa,
<lb n="658"/>e Tyolet dont reparla.
<lb n="659"/><q>« Dan chevalier, e cil, qui fu, 
<lb n="660"/>qui de l'espee fu feru,
<lb n="661"/>e qui fu cil qui l'en feri ? 
<lb n="662"/>Dites le moi, vostre merci. 
<lb n="663"/>Ce m'est a vis, ce fustes vos. »</q> 
<lb n="664"/>Cil s'embroncha, molt fu hontos.
<lb n="665"/><q>« Mes ce fu de bien fet col fret 
<lb n="666"/>qant vos feïstes tel forfet. 
<lb n="667"/>Bonement donné vos avoie
<lb n="668"/>le pié q'au cerf trenchié avoie, 
<lb n="669"/>e vos tel loier en sousistes, 
<lb n="670"/>pour <num>.I.</num> peu que ne m'oceïstes. 
<lb n="671"/>Mort en dui estre voirement. 
<lb n="672"/>Je vos donnai, or m'en repent ; 
<lb n="673"/>vostre espee que vos portastes 
<lb n="674"/>tres par mi le cors me boutastes ; 
<lb n="675"/>tres bien me cuidastes ocirre. 
<lb n="676"/>Se vos ce volez escondire 
<lb n="677"/>de prover voiant cest barnage, 
<lb n="678"/>au roi Artur en tent mon gage. »</q> 
<lb n="679"/>Cil entent qu'il dit verité, 
<lb n="680"/>du coup li a merci crié ; 
<lb n="681"/>plus doute la mort que la honte, 
<lb n="682"/>de rien ne contredit son conte. 
<lb n="683"/>Devant le roi a lui se rent 
<lb n="684"/>a fere son conmandement. 
<lb n="685"/>E Tyolet li pardonna 
<lb n="686"/>au conseil que il puis en a 
<lb n="687"/>du roi e de toz ses barons ; 
<lb n="688"/>e cil l'en vait a genoillons, 
<lb n="689"/>dont l'en eüst le pié besié 
<lb n="690"/>qant Tyolet l'a redrecié, 
<lb n="691"/>si l'en bese par grant amor ; 
<lb n="692"/>n'en oï puis parler nul jor.
<pb n="252"/>
<lb n="693"/>Li chevaliers le pié li rent 
<lb n="694"/>e Tyolet donques le prent 
<lb n="695"/>si l'a donné a la pucele. 
<lb n="696"/>Fleur de lis ou rose novele 
<lb n="697"/>qant primes nest el tans d'esté, 
<lb n="698"/>trespassoit ele de biauté. 
<lb n="699"/>Tyolet l'a donc demandee, 
<lb n="700"/>li rois Artur li a donnee, 
<lb n="701"/>e la pucele l'otroia ; 
<lb n="702"/>en son païs donc le mena. 
<lb n="703"/>Rois fu e ele fu roïne. 
<lb n="704"/>De Tyolet le lai ci fine.
				</ab>
</div>
</body>
</text>
</TEI>