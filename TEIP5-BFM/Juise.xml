<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader xml:lang="fr">
  <fileDesc>
    <titleStmt>
      <title type="main">Li ver del juïse</title>
      <title type="formal">Juise</title>
      <title type="reference">Juise</title>
      <title type="DEAF">JuiseR</title>
      <title type="gmd">transcription électronique</title>
      <author xml:id="anonyme">anonyme</author>
      <editor>Équipe de la Base de français médiéval - ENS de Lyon / UMR 5317 IHRIM</editor>
      <funder>Laboratoire IHRIM</funder>
      <funder>ANR (projet CoRPTeF)</funder>
    </titleStmt>
    <editionStmt>
      <edition>transcripion électronique</edition>
      <respStmt xml:id="encoder2">
        <resp>conversion en XML et balisage automatique</resp>
        <name xml:id="AL">Alexei Lavrentiev</name>
      </respStmt>
      <respStmt xml:id="proofreader2">
        <resp>deuxième relecture et vérification du balisage</resp>
        <name xml:id="BS">Bérénice Stoll</name>
      </respStmt>
      <respStmt xml:id="digitizer2">
        <resp>océrisation</resp>
        <name xml:id="CICL">CICL</name>
      </respStmt>
      <respStmt xml:id="encoder3">
        <resp>validation finale</resp>
        <name xml:id="LB">Lauranne Bertrand</name>
      </respStmt>
      <respStmt xml:id="proofreader1">
        <resp>première relecture</resp>
        <name xml:id="PK">Paul-Adrien Kompanietz</name>
      </respStmt>
    </editionStmt>
    <extent>- Numéros de pages saisies : de 51 à 71 ;
            - Nombre de mots d'après la base des descripteurs : <num xml:id="nb_mots_given">4256</num> ;
            - Nombre de tokens (mots et ponctuations) d'après la base des descripteurs : <num xml:id="nb_tokens_given">4886</num>.
    </extent>
    <publicationStmt>
      <publisher><name>Projet BFM, ENS de Lyon / UMR 5317 IHRIM</name></publisher>
      <address>
        <addrLine>15, parvis René Descartes</addrLine>
        <addrLine>69342 Lyon BP7000 Cedex 07</addrLine>
        <addrLine>Tél : 04 37 37 63 10</addrLine>
        <addrLine>Mèl : bfm@ens-lyon.fr</addrLine>
        <addrLine>http://bfm.ens-lyon.fr</addrLine>
      </address>
      <idno type="bfm">http://catalog.bfm-corpus.org/Juise</idno>
      <idno type="doi">10.34847/nkl.37b089er</idno>
      <availability status="free">
        <licence target="https://www.etalab.gouv.fr/licence-ouverte-open-licence">
          <ab type="availability_bfm" subtype="libre_1d">Les Conditions générales d'utilisation de la Base de Français Médiéval (http://txm.bfm-corpus.org/bfm/files/CGU.pdf), 
            et notamment son Article 5 « PROPRIETE INTELLECTUELLE », s'appliquent à tous les textes de la BFM.</ab>
          <ab type="how_to_cite">
            <label>Comment citer ce texte :</label>
            <bibl>Li ver del juïse, édité par Erik Rankka, Uppsala, 1982. 
              Publié en ligne par la Base de français médiéval, http://catalog.bfm-corpus.org/Juise.
              Dernière révision le 2010-08-10.</bibl>
            D'autres formules de citation de la BFM sont proposées sur la page web suivante :
            http://bfm.ens-lyon.fr/article.php3?id_article=281.</ab>
          <ab type="availability_file">
            <label>Conditions d'utilisation de ce fichier :</label>
              Ce fichier numérique est composé du corps de texte médiéval et des suppléments numériques : balisage XML-TEI, métadonnées (teiHeader), balisage du discours direct. Le texte et les suppléments numériques sont libres de droit et peuvent à ce titre être téléchargés et réutilisés librement.
            </ab>
            <ab type="availability_txmweb" subtype="illimité">
              <label>Conditions d'utilisation par le portail BFM :</label>
              Pas de limitation dans l'usage des fonctionnalités. Téléchargement autorisé.</ab>
        </licence>
      </availability>
    </publicationStmt>
    <sourceDesc>
      <biblFull>
        <titleStmt>
          <title>Li ver del juïse. Sermon en vers du XIIe siècle</title>
          <respStmt>
            <resp xml:id="editor">Éditeur scientifique</resp>
            <name type="person">Erik Rankka</name>
          </respStmt>
        </titleStmt>
          <extent>21 page(s), soit 51-71
        </extent>
        <publicationStmt>
          <publisher>Almqvist och Wiksell</publisher>
          <pubPlace>Uppsala</pubPlace>
          <date>1982</date>
          <idno type="sigle_DEAF">JuiseR</idno>
          <idno type="cote_biblio">D4.019 : COR3-RAN</idno>
        </publicationStmt>
      </biblFull>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Projet :<name type="project">BFM - Base de français médiéval</name>
        Resp. : <name type="person">Céline Guillot-Barbance</name>
        Équipe : Base de français médiéval
        Laboratoire : UMR 5317 IHRIM
        Institution : ENS de Lyon
        <address>
          <addrLine>15, parvis René Descartes</addrLine>
          <addrLine>69342 Lyon BP7000 Cedex 07</addrLine>
          <addrLine>Tél : 04 37 37 63 10</addrLine>
          <addrLine>Mèl : bfm@ens-lyon.fr</addrLine>
          <addrLine>http://bfm.ens-lyon.fr</addrLine>
        </address>
      </p>
    </projectDesc>
    <editorialDecl>
      <p>Voir le <ref target="http://bfm.ens-lyon.fr/IMG/pdf/Manuel_Encodage_TEI.pdf">Manuel d'encodage XML-TEI de la BFM</ref>
        et les <ref target="http://bfm.ens-lyon.fr/IMG/pdf/Consignes_BFM.pdf">Consignes aux relecteurs de la BFM</ref>.</p>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="ab" occurs="24"/>
        <tagUsage gi="body" occurs="1"/>
        <tagUsage gi="div" occurs="3"/>
        <tagUsage gi="foreign" occurs="1"/>
        <tagUsage gi="front" occurs="1"/>
        <tagUsage gi="hi" occurs="2"/>
        <tagUsage gi="lb" occurs="484"/>
        <tagUsage gi="milestone" occurs="18"/>
        <tagUsage gi="note" occurs="2"/>
        <tagUsage gi="p" occurs="3"/>
        <tagUsage gi="pb" occurs="21"/>
        <tagUsage gi="q" occurs="19"/>
        <tagUsage gi="supplied" occurs="1"/>
        <tagUsage gi="text" occurs="1"/>
      </namespace>
    </tagsDecl>
  </encodingDesc>
  <profileDesc>
    <creation>
      <name type="author">anonyme</name>
      <title>Li ver del juïse</title>
      <date type="compo" when="1137-01-01" notBefore="1125-01-01" notAfter="1150-01-01" n="12">2e q. 12e s.</date>
      <date type="compo_periode">ancien</date>
      <date type="compo_sous_siecle" n="12_2">milieu</date>
      <date type="ms" when="1216-01-01" notBefore="1200-01-01" notAfter="1233-01-01">déb. 13e s.</date>
      <idno type="msIdentifier">Oxford, Bodl., Canonici Misc. 74</idno>
      <region type="dialecte_auteur">liégeois</region>
    </creation>
    <langUsage>
      <language ident="fro" usage="100">Le texte est entièrement écrit en vers en ancien français.</language>
    </langUsage>
    <textDesc>
      <channel mode="w">print</channel>
      <constitution type="single"/>
      <derivation type="reprise">Bible et version longue latine</derivation>
      <domain type="religieux"/>
    <factuality type="inapplicable"/>
      <interaction type="none"/>
      <preparedness type="prepared"/>
      <purpose type="narrative"/></textDesc>
    <textClass>
      <keywords scheme="http://authorities.bfm-corpus.org">
        <term type="domaine">religieux</term>
        <term type="genre">sermon</term>
        <term type="forme">vers</term>
        <term type="relation">reprise</term>
      </keywords>
    </textClass>
  </profileDesc>
  <revisionDesc>
    <change who="#CICL" when="2008-09-02">océrisation</change>
    <change who="#CICL" when="2008-09-02">scannage</change>
    <change who="#AL" when="2008-12-17">préparation à la relecture 1</change>
    <change who="#PK" when="2008-12-18">première relecture</change>
    <change who="#AL" when="2009-01-14">création du descripteur</change>
    <change who="#AL" when="2009-02-16">conversion en XML et balisage automatique</change>
    <change who="#BS" when="2010-06-19">deuxième relecture et vérification du balisage</change>
    <change who="#LB" when="2010-08-10">validation finale</change>
    <change when="2010-08-10">Dernière modification du texte</change>
    <change when="2022-11-04">Création de cet entête</change>
  </revisionDesc>
</teiHeader>
<text>
        <pb n="51"/>
		<front>
		    <div type="titre">
		        <p>Li ver del juïse</p>
		    </div>
		</front>
		<body>
			<div type="version" n="1">
			  <note resp="editor" place="inline">A = <hi rend="ital">Oxford, Bibl. Bodl., Canonici Misc. 74.</hi></note> <milestone unit="folio" n="131r"/>
				<ab type="gv">
<lb n="1" rend="alinea"/>Sanior, oiez raison glorïose et saintisme!
<lb n="2"/>Del ciel en est la voiz, de paradis la vie.
<lb n="3"/>Deus la tremist en terre por amendeir noz vies,
<lb n="4"/>Kar tot sumes turneit a dol et a martyre.
<lb n="5"/>Nos n'avons cariteit, la foiz s'en est fuïe;
<lb n="6"/>Aleie en est en ciel, tote l'avons guerpie.
<lb n="7"/>Perdue avons la loi ke donat nostre sire,
<lb n="8"/>Ne nos ramembret mie del grant jor del juïse.
<lb n="9"/>Or oiez la raison, c'onques teiz ne fut dite,
<lb n="10"/>Ne teiz nen iert ja mais, por tant ke secles vive.
<lb n="11"/>Uns angles l'aportat de la terre d'Egypte,
<lb n="12"/>Faite fut en Egipte la u Deus fut nurriz;
<lb n="13"/>Par la boche d'un angle la fist uns sainz hermiz
<lb n="14"/>En dis et set anees ke unkes pain ne vit, <milestone unit="folio" n="131v"/>
<lb n="15"/>Ne ne manjat de char et si ne biut de vin,
<pb n="52"/>
<lb n="16"/>Mais de la glore Deu fut totens raempliz.
<lb n="17"/>Or vos mande del ciel la virgene Marie:
<lb n="18"/>Se vos bien entendeiz ceste raison saintisme,
<lb n="19"/>Corone en porterez devant Deu al juïse.
<lb n="20"/>Ci comence de Deu teil raisons a venir,
<lb n="21"/>Se vos bien l'entendeiz, s'en avreiz paradis,
<lb n="22"/>Corone en porterez devant Deu al juïs
<lb n="23"/>Et Deus le vos otroit par la sue mercit.
				</ab>
			    <ab type="gv">
<lb n="24" rend="alinea"/>Des or mais vos dirai del grant jor del juïs
<lb n="25"/>Quant Deus repairerat et li munz iert finis;
<lb n="26"/>Il n'iert d'or ne d'argent, de casteaz ne de cis,
<lb n="27"/>Mais de noz, pecheors, qui Deu n'avrons serviz,
<lb n="28"/>Car tuit repairerons devant Deu al juïs.
<lb n="29"/>Teilz angoisse ne fut puis ke Deus lo ciel fist
<lb n="30"/>Cum serat icel jor ke terre prendrat fin.
<lb n="31"/>Perdut avrons l'orgul doleros c'avons ci
<lb n="32"/>Et les lasses de femes et manches et traïns,
<lb n="33"/>Manteaz et garnimenz, dont eles funt teil pris;
<lb n="34"/>N'avront aneaz elz doiz ne harpons al col mis,
<lb n="35"/>Ne braz avant jeteit par orgulh ne par pris.
<lb n="36"/>Par orgul perdit l'angles et Deu et paradis,
<lb n="37"/>Car si ferunt tot cil qui en orgul seront pris.
<lb n="38"/>Plus at feme d'orgul, si cum la letre dis,
<lb n="39"/>Ke n'ait hom nuz en terre qui de mere soit vis;
<pb n="53"/>
<lb n="40"/>Tant embracent richeces dont eles funt tel pris,
<lb n="41"/>L'une le fait por l'atre par orgul et par pris.
<lb n="42"/>Ce n'est mie por Deu ne ameir ne servir,
<lb n="43"/>Ne por les lasses d'anrmes sorcurre ne garir, <milestone unit="folio" n="132r"/>
<lb n="44"/>Mais por enfer conquerre, embracier et tenir,
<lb n="45"/>C'anzois i soient enz ke li puiz sot empliz
<lb n="46"/>Et avoir granz angoisses ke veniet li juïs.
<lb n="47"/>La mors iert obliee qui nos mentenrat si;
<lb n="48"/>Puis ke ele venrat iciz jois iert guerpis,
<lb n="49"/>Petit en porterons, las nos! cant a la fin.
<lb n="50"/>En un dolent drapel iert li cors sepeliz,
<lb n="51"/>Buteiz iert en la terre as nuz vers poverins,
<lb n="52"/>Ki manjeront la char et trencherunt par mi
<lb n="53"/>Et si rorunt les olz et la boche et lo vis.
<lb n="54"/>Li orguelz de cest secle revient a petit pris
<lb n="55"/>Et cant la mors prent l'omme, li cors vait angoissant,
<lb n="56"/>La lasse d'anrme vait par tot lo cors fuiant.
<lb n="57"/>Li diable l'en chacent mut angoissosement,
<lb n="58"/>Fierement la requierent et derrier et devant,
<lb n="59"/>Ses doleros pechiez li vunt renovelant
<lb n="60"/>Et sa lasse de vie c'at demeneie tant.
<lb n="61"/>Sor lo piz li assïent a la boche devant :
<lb n="62"/>(Si dïent a la lasse que bien l'ot et entent)
<lb n="63"/><q>« Za fors en venreiz vos, car ne poeiz garir,
<pb n="54"/>
<lb n="64"/>Hui est venuz li jors k'en ireiz en exilh.
<lb n="65"/>Li cors vos at traïe, car unkes bien ne fist,
<lb n="66"/>Nen ot ainc cariteit ne de povre mercit,
<lb n="67"/>Ne ainc n'amat parole qui de Deu part venist.
<lb n="68"/>En enfer vos moinrai, lo doleros païs,
<lb n="69"/>Ne revenreiz ja mais parleir a voz amis,
<lb n="70"/>Voz granz dolors conteir, ke soferrez todis. »</q>
<lb n="71"/>La mors est angoissose et l'anrme est emarie,
<lb n="72"/>Ele voit les diables qui ne s'atargent mie.
<lb n="73"/>L'anme huchet el cors: <q>« Damme sainte Marie, <milestone unit="folio" n="132v"/>
<lb n="74"/>Car moi sorcoreiz hui, glorïose roïne! »</q>
<lb n="75"/>Li diable li mostret sa dolerose vie,
<lb n="76"/>En sa main tient la chartre ke il en at escrite.
<lb n="77"/><q>« Lasse, por koi l'apeles? de sorcurs n'avras mie!
<lb n="78"/>Tant cum tu fus al secle Deu ne volsis servir,
<lb n="79"/>Ne la sainte virgine u Deus sanc et char prist.
<lb n="80"/>Teil chose as deservie qui griés t'iert assofrir,
<lb n="81"/>Ne toi faldront angoisses que n'en aies todis.
<lb n="82"/>Tu ne fus osteliers, almones ne fesis,
<lb n="83"/>Asseiz manjas al secle et ton ventre remplis,
<lb n="84"/>Petit en donas Deu, or lo trueves ici,
<lb n="85"/>Ne verras mais la joie qui est en paradis.
<lb n="86"/>Vos quidastes lo munde enbracier et tenir,
<lb n="87"/>La mors vos est mut pres, ne porez ja garir,
<lb n="88"/>Ultre s'en est aleie, tart est del repentir,
<pb n="55"/>
<lb n="89"/>Ne vos faldront angoisses ke n'en aiez todis.
<lb n="90"/>Li avoirs est remeis et as filz et az filhes,
<lb n="91"/>Il en vivront al secle, vos en ireiz chaitive :
<lb n="92"/>Ne vos sorcurrat mie la virgine Marie
<lb n="93"/>Et sainz Michiez, li angles, ne vos conduira mie;
<lb n="94"/>Ja ne vos faldront paines a nul tens ke Deus vive.
<lb n="95"/>Or penras penitance, c'onkes teil ne fesis,
<lb n="96"/>Par fou et par tormenz et par doleros lius.
<lb n="97"/>Les granz fains et les sois vos covenrat sofrir,
<lb n="98"/>La noif et la jeleie et les dolenz gresis,
<lb n="99"/>En roge meir la grant la passerez par mi,
<lb n="100"/>Ne vos faldront angoisses, anz venrat li juïs.
<lb n="101"/>Cant Deus repairerat, lo jor sereiz maldit
<lb n="102"/>En la chartre d'enfeir qui ja ne prendrat fin. »</q> <milestone unit="folio" n="133r"/>
			    </ab>
			    <ab type="gv">
<lb n="103" rend="alinea"/>Or oiez que dist l'anme de cel cors que guerpist :
<lb n="104"/><q>« Cors, maldiz soies tu, ensi cum Deus te fist,
<lb n="105"/>Et toi oil et tes mains et ta boche et tes vis,
<lb n="106"/>Toi piet, ta monumente et quant ke est de ti,
<lb n="107"/>Cant ce as manovreit et ce as deservit
<lb n="108"/>K'en irai en enfer, lo doleros païs.
<lb n="109"/>Ja ne l'ai je forfait, beaz Deus! ne deservit,
<lb n="110"/>Mais ciz lerres de cors qui unkes bien ne fist;
<pb n="56"/>
<lb n="111"/>Ainc nen ot cariteit ne de povre mercit,
<lb n="112"/>Ne n'amat la parole qui de Deu part venist. »</q>
<lb n="113"/>Vullet u non la lasse, il l'estuet fors issir.
<lb n="114"/>Li cors remaint en terre, l'anme en vait en exil.
			    </ab>
			    <ab type="gv">
<lb n="115" rend="alinea"/>Diable ne manjouent ne pain ne char ne vin,
<lb n="116"/>Mais les anmes des cors ke Dammledeus i mist :
<lb n="117"/>Celes ardent, devorent et detrenchent par mi
<lb n="118"/>Des le lundi al main entrosc'al semedi
<lb n="119"/>Endroit le cok cantant, dont repaire li spirs,
<lb n="120"/>Sor la tombe s'asïet la u li cors se gist.
<lb n="121"/>Se li cors at bien fait et il est en bien pris,
<lb n="122"/>L'anme li rent de Deu et grasces et mercis :
<lb n="123"/><q>« Cors, benoiz soies tu! bon sanior ai servit,
<lb n="124"/>J'ai veüe la joie qui est en camp florit,
<lb n="125"/>Vie manrai toz jors avoc les Deu amis.
<lb n="126"/>Je m'en irai, beaz sire, Deus soit garde de ti!
<lb n="127"/>A la joie enterrai qui est en camp flori,
<lb n="128"/>La venront trestuit cil qui avront Deu servit. »</q>
			    </ab>
			    <ab type="gv">
<lb n="129" rend="alinea"/>Se li cors at mal fait et il est en mal pris,
<lb n="130"/>L'anme revient al cors, durement lo maldist :
<pb n="57"/>
<lb n="131"/><q>« Lerres, que faites vos? Ke giseiz vos ici? <milestone unit="folio" n="133v"/>
<lb n="132"/>Ja ne poeiz troveir ne osteil ne abit,
<lb n="133"/>Unkes ne herbrijastes ne povre ne mendit,
<lb n="134"/>Et Deus nel ferat vos el sien saint paradis.
<lb n="135"/>Ki vos ferait garanz devant Deu al juïs?
<lb n="136"/>Ki ki ait beneizon, vos i sereiz maldis.
<lb n="137"/>Veeiz les granz companies des diables ici!
<lb n="138"/>Parmi la roge meir m'en cacent en exil,
<lb n="139"/>Lassus devers lo ciel jus me laissent caïr.
<lb n="140"/>Veeiz ces grandes plaies ke moi estuet tenir!
<lb n="141"/>Ju ai es piez chaaines et el col teil laz mis,
<lb n="142"/>Lerres, plus sunt pesant ke n'est la turz David;
<lb n="143"/>La turz de Babiloine ne poiset mie si
<lb n="144"/>Et toz tens moi creistront, ke ja ne prendront fin. »</q>
<lb n="145"/>La ne rireiz vos mais dont venistes ici,
<lb n="146"/>Ne ja mais ne verreiz ne parent ne amis
<lb n="147"/>Ez doleros tormenz que vos estuet sofrir.
<lb n="148"/>De trestotes ces anmes ki cest secle ont guerpit
<lb n="149"/>Ne seit ne clers ne moines tant canteir ne tant lire
<lb n="150"/>Ke nuz puist onkes dire u cez anmes vertisent,
<lb n="151"/>Ne u prendent osteil, ne u soit li lor vivres.
<lb n="152"/>Volentiers revenroient por lur angoisses dire,
<lb n="153"/>Ja ne revenront mais, anz venrat li juïses.
<lb n="154"/>La venront li dolent qui Deu n'avront servi,
<lb n="155"/>Ki n'orent cariteit ne de povre merci;
<pb n="58"/>
<lb n="156"/>Cil relevront lo jor ke Deus devrat venir,
<lb n="157"/>Dont mosterrat ses plaies ke il por nos sofri.
<lb n="158"/>Comment ke li plaiz prendet, il n'avront ja merci,
<lb n="159"/>Car Deus les maldirat qui onkes ne mentit,
<lb n="160"/>En la chartre d'infer qui ja ne prendra fin. <milestone unit="folio" n="134r"/>
<lb n="161"/>Oï, sanior et dames! com laide atente at ci,
<lb n="162"/>Ki voz cors aveiz tant et gardeiz et nurriz.
<lb n="163"/>Par la mort ancoissose i passereiz par mi,
<lb n="164"/>A grant dol revenront cist las cors a la fin.
<lb n="165"/>Hom non est en cest secle tant amez et serviz,
<lb n="166"/>Tant ait or ne argent ne casteaz ne païs,
<lb n="167"/>Puis ke l'anme en est fors, ke il ne soit toz viz,
<lb n="168"/>Ne tant l'aimet nuz hom qu'il lo vulet veïr.
<lb n="169"/>Bien encovienie a vers puis qu'il est enfoïz!
<lb n="170"/>Ciz secles nos at toz traïz et avogleiz,
<lb n="171"/>Hom non est en cest secle tant serviz ne amez,
<lb n="172"/>Tant ait or ne argent ne chasteaz ne citeiz,
<lb n="173"/>Pelizons vairs et gris et manteaz engoleiz,
<lb n="174"/>Puis ke l'anme en est fors, qu'il ne soit avileiz.
<lb n="175"/>Ohi de noz las cors que nos par amons si!
<lb n="176"/>De boivre et de mangier, de cacier, de vestir,
<lb n="177"/>Des deliz de cest secle li faisons son plaisir.
<lb n="178"/>Cum plus l'engrasserons et ferons son delit,
<lb n="179"/>Plus en rorunt li ver cant venrat a la fin.
<lb n="180"/>Tant vit li hom al secle cum Deu vient a plaisir,
<lb n="181"/>Et cant la mors li prent, li cors angoisse si
<lb n="182"/>Ke les veines del cuer li detrenchent par mi.
<pb n="59"/>
<lb n="183"/>Grant angoisse at li cuers cant l'anrme en doit issir,
<lb n="184"/>Donc remaint li orguez et li doleros pris
<lb n="185"/>Et la lasse de vie ke nos maintenons ci.
			    </ab>
			    <ab type="gv">
<lb n="186" rend="alinea"/>Kant Deus repairerat al grant jor del juïs,
<lb n="187"/>Il nos demanderat cum nos l'avons servit.
<lb n="188"/>Gardons ke nos soions porpenseit et garnit,
<lb n="189"/>Ke ce aions ovreit ke Deu vienie a plaisir <milestone unit="folio" n="134v"/>
<lb n="190"/>Et respondre puisons devan Deu al juïs.
<lb n="191"/>La ne prenderat nuz ne conseil ne respit,
<lb n="192"/>Tuit li bon avoc lui iront en paradis,
<lb n="193"/>Et li mal en infer u ja ne prendront fin.
<lb n="194"/>A Deu li noz orguelz lo jor par iert si vilz
<lb n="195"/>Ke nos n'avrons vestit pelizons vairs ne gris,
<lb n="196"/>Ne les lasses de femes ne mantealz ne traïns.
<lb n="197"/>Ja ne serons a plait, se Deu n'avons servit,
<lb n="198"/>Des pechiez k'avons fait, parveüt et garnit.
<lb n="199"/>Al cors parlerat l'anrme al jor del grant juïs :
<lb n="200"/><q>« Ohi, cors doleros! por koi t'ai tant servit
<lb n="201"/>Et doneit a mangier et culchiet en bon lit?
<lb n="202"/>Se bien eüsses fait, or lo trovasses ci,
<lb n="203"/>Or irai en infer, lo doleros païs,
<lb n="204"/>La grant dolor d'infer moi covenrat soffrir. »</q>
<lb n="205"/>La trembleront li mal lo jor par teil aïr
<lb n="206"/>Ke parmi les genchives entrosk'as sobreciz
<lb n="207"/>Venront les denz ensemble d'angoisse del juïs.
<pb n="60"/>
<lb n="208"/>(La iert li reprochiers de Damledeu servir.)
			    </ab>
			    <ab type="gv">
<lb n="209" rend="alinea"/>Or oiez de cele anme ki Deu avrat servit
<lb n="210"/>Et qui out cariteit et de povre mercit.
<lb n="211"/>Blans vestimenz avrat, glorïos et floriz,
<lb n="212"/>Ki plus reluiront cleir ke soloz n'esclarcit :
<lb n="213"/>Ce iert senefiance k'ele avrat paradis.
<lb n="214"/>Prendeiz garde as bons sainz qui sorent Deu servir
<lb n="215"/>Et al mund preecherent et reciurent martyr;
<lb n="216"/>As povres besinos penerent de servir,
<lb n="217"/>N'orent cure d'avoir encontre Deu tenir.
<lb n="218"/>Or sunt riche et manant de la celeste vie, <milestone unit="folio" n="135r"/>
<lb n="219"/>Li cors en sunt servit en mostier et en glise;
<lb n="220"/>Teil joie ont devant Deu ke nuz clers ne seit dire,
<lb n="221"/>Car si avront tuit cil qui feront son servise.
<lb n="222"/>La serunt li bon povre devant Deu en tel pris,
<lb n="223"/>Ki orent fain et soit por lo grant Deu servir.
<lb n="224"/>Ki kis ait decaciez, Deus nes avrat pas viz,
<lb n="225"/>Blans vestimenz avront, glorïos et floriz.
<lb n="226"/>Ki ki fallet a siege, ilh i seront assis,
<lb n="227"/>Les sieges Damledeu avront en paradis.
			    </ab>
			        
<pb n="61"/>
			    <ab type="gv">
<lb n="228" rend="alinea"/>Ce reconte sainz Polz, ki les duelz d'enfer vit :
<lb n="229"/>Cant entrat en enfer por l'angoisse veïr,
<lb n="230"/>Iluec trova un home et une feme ardenz.
<lb n="231"/>Parmi les cors avoient nonante nuef tormenz,
<lb n="232"/>Li flanc et li costeit ardoient as dolenz,
<lb n="233"/>Diable lur ardoient les boches et les vis.
<lb n="234"/>Sainz Polz les apelat, or oiez k'il lur dist :
<lb n="235"/><q>« Diva, contes a moi ton tres doleros lit
<lb n="236"/>Et ta tres grant dolor qui ja ne prendrat fin. »</q>
<lb n="237"/><q>« Or entendeiz, beaz sire, la nostre male vie,
<lb n="238"/>Car adés pluet sor nos et givlet et gresilhet,
<lb n="239"/>Ne nos falent dolors, angoisses ne martyre.
<lb n="240"/>Uns vens nos vient corant qui vient dever l'abi,
<lb n="241"/>Et si par est tant fors toz nos trenchet par mi
<lb n="242"/>Et les cors met a mal des doleros chaitis;
<lb n="243"/>Ja por tant cum Deus vivet nostre mal n'avront fin.
<lb n="244"/>Set turs at en enfer qui sunt del tens antif,
<lb n="245"/>Nonante nuef tormenz at en chascun'assis,
<lb n="246"/>Et en l'un'et en l'atre jetet hom les chaitis.
<lb n="247"/>Tant i at des angoisses nen iert conté ne dit. <milestone unit="folio" n="135v"/>
<lb n="248"/>Car lo racontes, sire, les dolanz qui sunt vif;
<lb n="249"/>Ki ki ait beneizon, li mal i sunt falhit. »</q>
<lb n="250"/>Or vos dirai d'enfer, lo doleros païs,
<pb n="62"/>
<lb n="251"/>Cum fut faiz et creeiz et storeiz et assis.
<lb n="252"/>Cil qui gardent enfer furent de paradis,
<lb n="253"/>Si furent orguillos envers Deu qui les fis,
<lb n="254"/>Jus les en avalat et enfer en emplis.
<lb n="255"/>Hom n'i puet troveir fonz et terre n'i prent fins,
<lb n="256"/>Ce reconte sainz Poz qui les duez d'enfer vis.
<lb n="257"/>La irunt tuit icil ki Deu avront guerpit.
<lb n="258"/>Il i at nuit obscure ke om n'i puet veïr,
<lb n="259"/>Onkes n'i luist soloz, ne jors n'i esclarcist,
<lb n="260"/>Ne ja ne ferat mais, por tant cum Deus soit vis.
<lb n="261"/>Cant diable repairent de querre les chaitis,
<lb n="262"/>En enfer si s'en rentrent, lo doleros païs,
<lb n="263"/>Li uns manjoue l'autre et detrenche par mi.
<lb n="264"/>Mut sunt lait li sanior ki gardent cel païs,
<lb n="265"/>Les goules ont baees, dont li tenebrors ist.
<lb n="266"/>Ceps i at et charcanz et mut doleros liz
<lb n="267"/>Et buies et chaainnes as anmes comburir.
<lb n="268"/>Li serpent sunt avoc, qui detrenchent par mi;
<lb n="269"/>Une aiwe i at corant, d'enfer en eist li rius.
<lb n="270"/>- Ce recunte sainz Poz, ki les duez d'enfer vit. -
<lb n="271"/>La turs de Babilone, s'astoit fers et aciers,
<lb n="272"/>En une petite hore l'avroit par mi trenchit.
<lb n="273"/>La baniet hom trois foiz cascon jor les chaitis,
<lb n="274"/>En aprés les met hom ez caldieres bolir
<lb n="275"/>Et puis les remet hom sor les rosteaz rostir.
<lb n="276"/>Ci comencent angoisses dolentes a venir. <milestone unit="folio" n="136r"/>
<lb n="277"/>Teil fain at en infer vos ja porreiz oïr,
<lb n="278"/>Ke por trestot lo pain c'unkes Dammledeus fist,
<lb n="279"/>Des k'il format Adam entroske al juïs,
<lb n="280"/>S'il l'avoient mangiet et lo mont transglotit,
<lb n="281"/>N'avroient en enfer lor grant fain aemplit.
<lb n="282"/>En aprés ont teil soit cum ja poreiz oïr,
<lb n="283"/>S'avoient la meir biute et les aiwes en fin
<pb n="63"/>
<lb n="284"/>Et lo ciel et la terre avoient englotit,
<lb n="285"/>N'avroient en infer lor grant soit acomplit.
<lb n="286"/>Iteilz sunt les angoisses ke elz estuet sofrir.
<lb n="287"/>Mut ont grant deseier del grant Deu a veïr,
<lb n="288"/>Nel puent oblier ne par nuit ne par dis,
<lb n="289"/>Il nel verront ja mais, por tant cum Deus soit vis.
			        </ab>
			        <ab type="gv">
<lb n="290" rend="alinea"/>Cant Deus repairerat al grant jor del juïs,
<lb n="291"/>Ce nen iert mie plaiz qui soit mis en respit.
<lb n="292"/>L'uns ne dirat a l'atre : <q>« Amis, aidiez me ci,
<lb n="293"/>S'irons a Damrideu, si li prirons mercit. »</q>
<lb n="294"/>Volentiers s'en fuiroient, sez astoit consentit.
<lb n="295"/>Ja nen movront les piez, la boche ne lo vis,
<lb n="296"/>Ne ne clorunt lor oil, ne seront tant hardit.
<lb n="297"/>En teil osteil irat qui Deu avrat guerpit,
<lb n="298"/>De nonante nuef plaies iert chascon jor serviz.
<lb n="299"/><supplied resp="#editor" rend="crochets">Qui fist que enfanz furent et que avant ne vindrent,
<lb n="300"/>Deu en demandera les aimes et les vies,
<lb n="301"/>Se par fort penitence n'ait eu le cors martire.</supplied>
<lb n="302"/>Et li petit enfant qui ensi sunt murdrit
<lb n="303"/>A tort et a pechiet et noiet et ocis,
<lb n="304"/>Cil iront tuit ensemble devant Deu al juïs,
<lb n="305"/>Clameront soi a Deu qui ciel et terre fist;
<lb n="306"/>Cil lor en ferat droit qui onkes tort ne fist.
<lb n="307"/>Grans tormenz en avront cil qui les ont murdriz
<lb n="308"/>En la chartre d'infer u ja ne prendront fin. <milestone unit="folio" n="136v"/>
<lb n="309"/>La avrat tante femme et tant home enmari
<pb n="64"/>
<lb n="310"/>Et tant faz crestoien qui Deu n'avront servi
<lb n="311"/>Des richesces qu'il orent et des avoirs colhiz,
<lb n="312"/>Ke il doneir deüssent as povres et mendis.
<lb n="313"/>Asseiz avront angoisses, ja non iert contredit,
<lb n="314"/>Mis serunt en la chartre u Herodes fut mis.
<lb n="315"/>N'i avrat saint ne sainte, apostle ne martyr,
<lb n="316"/>Ne angle ne archangle ke onkes Deus fesist
<lb n="317"/>Ki d'userier apele Damrideu al juïs,
<lb n="318"/>S'il est pris en usures, ke Deus en ait mercit,
<lb n="319"/>Ne ja eisset d'infer, por tant cum Deus soit vis.
			    </ab>
			    <ab type="gv">
<lb n="320" rend="alinea"/>Ohi! dolenz orguez, quant home avras traït!
<lb n="321"/>Quantes beles joventes as tolut paradis
<lb n="322"/>Et quant bel baceleir, jovencel et mechin
<lb n="323"/>Feras ardoir el fou u ja ne prendront fin
<lb n="324"/>Et freres et serors et uxors et mariz !
<lb n="325"/>Ne fut ainc si granz duelz puis ke Deus l'angle fist
<lb n="326"/>K'il serat a cel jor ke seront departit.
<lb n="327"/>La soi departeront et parent et ami,
<lb n="328"/>La guerpirat li peres et la mere son fil.
<lb n="329"/><q>« Oi, filz », dirat la mere, « dolente vos guerpis. »</q>
<lb n="330"/><q>« Et je vos, bele mere, et chaitis et mariz,
<lb n="331"/>Bien nos fut acointiet teiz seroit nostre fins,
<lb n="332"/>Ki Deu ne serviroit ja n'avroit paradis. »</q>
<lb n="333"/>Veeiz ici lo regne u remanront toz dis,
<lb n="334"/>Dont lor vient fous et flame qui art tot lo païs.
<lb n="335"/>Ci remanront ja mais, por tant cum Deus soit vis.
<pb n="65"/>
<lb n="336"/>Cum lasse desevrance, las nos! tant at ici.
<lb n="337"/>Tuit li terrien sanior ke nos avrons servi, <milestone unit="folio" n="137r"/>
<lb n="338"/>Il nos faldront trestot cant venrat al juïs,
<lb n="339"/>Li plaiz n'iert mie lur lo jor a maintenir,
<lb n="340"/>Anz iert al creator qui trestot lo mont fist.
<lb n="341"/>En une chartre anciene el parfunt seront mis,
<lb n="342"/>Unkes n'i luist soloz, ne jors n'i esclarcist;
<lb n="343"/>La entrat nostre sire por noz anmes garir.
<lb n="344"/>Tant i at de parfunt, si cum li angles dist,
<lb n="345"/>Cum del ciel a la terre et ke ferons, chaitif?
<lb n="346"/>Mut par iert grant li duelz devant Deu al juïs;
<lb n="347"/>La seront clerc et moine et paien et juïs
<lb n="348"/>Et li faz crestijen ki Deu n'avront servis.
<lb n="349"/>U iert pris cil avoirs que la iert departiz?
			    </ab>
			    <ab type="gv">
<lb n="350" rend="alinea"/>Or oiez des granz signes qui de devant venront :
<lb n="351"/>Li ciez abaisserat a la terre parfunt,
<pb n="66"/>
<lb n="352"/>La terre crollerat trosk'en abisme el fonz,
<lb n="353"/>Li soloz et la lune lur beateit changeront,
<lb n="354"/>Li jors devenrat nuiz et del ciel plovrat sons,
<lb n="355"/>Ne fut ainc si granz duez puis ke Deus fist Adam.
<lb n="356"/>Li halt munt enz ez aiwes d'angoisses verseront,
<lb n="357"/>Les estoiles del ciel jus a terre cheront.
<lb n="358"/>Dont venrat une nue devers lo ciel amont,
<lb n="359"/>Ki iert tant tenebrose por l'angoisse de nos.
<lb n="360"/>Si en revenrat une de devers orïent
<lb n="361"/>Ki jetrat fou et flamme mut angoissosement
<lb n="362"/>Nuef costes environ, ardant terre en toz senz;
<lb n="363"/>Non iert ja mais mostiers ne vile ne païs,
<lb n="364"/>Nen iert bors ne casteaz ke unkes Deus fesist,
<lb n="365"/>La mers s'en iert fuïe et li monz iert finiz.
			    </ab>
			    <ab type="gv">
<lb n="366" rend="alinea"/>Or oiez la novele del grant jor del juïs : <milestone unit="folio" n="137v"/>
<lb n="367"/>Il sonerat en ciel mes sires sainz Mathius,
<lb n="368"/>Ensemble revenront vostre chars et voz spirs,
<lb n="369"/>De la terre eissereiz doleros et eschis,
<lb n="370"/>L'uns regarderat l'autre, toz sereiz embahis.
<lb n="371"/>Ne fut ainc si granz duez puis ke Deus l'ome fist,
<lb n="372"/>Cum serat a cel jor cant terre prendrat fin.
<lb n="373"/>L'endemain sonerat Marcus en orïent,
<pb n="67"/>
<lb n="374"/>Tuit relevront de terre la crestoiene gent.
<lb n="375"/>Al tierc jor sonerat mes sires sainz Johans.
<lb n="376"/>Lo jor verreiz miracles mut mervillos et grans,
<lb n="377"/>La quarte part del ciel derrompre et departir.
<lb n="378"/>Al quart jor sonerat Lucas en camp florit,
<lb n="379"/>Trestuit istront de terre, ja uns nen iert a dire :
<lb n="380"/>Li clerc, li lai, li moine convers et li ermite
<lb n="381"/>Et li faz crestïen qui Deu avront guerpit,
<lb n="382"/>Tot en iront al plait devan Deu al juïs.
<lb n="383"/>La porat Deu veoir qui onkes mais nel vit
<lb n="384"/>Et les plaies avoc que il por nos sofrit.
			    </ab>
			    <ab type="gv">
<lb n="385" rend="alinea"/>Or oiez ke dirat nostre sire a la fin :
<lb n="386"/><q>« Ge vos ai toz refaiz et ameneiz ici
<lb n="387"/>Et si sui revenuz al terme que vos mis.
<lb n="388"/>Se bien eüssiez fait, or lo trovisiez ci.
<lb n="389"/>Veeiz ici les plaies ke jo por vos sofri,
<lb n="390"/>Les palmes et les piez en ou perciés par mi.
<lb n="391"/>Grant angoisse out mes cors kant m'anme en diet issir,
<lb n="392"/>Mes cors fut consumeiz, s'en alat mes espirs
<lb n="393"/>Enz el parfunt enfer por voz anmes garir.
<lb n="394"/>Les miens en jetai fors, c'un sol n'i relenki,
<lb n="395"/>Et si laissai infer trosk'al jor del juïs. <milestone unit="folio" n="138r"/>
<lb n="396"/>Ce soffri ge por vos, k'aveiz soffert por mi?
<lb n="397"/>Mes cors remeist en terre, la u fut crucifix,
<lb n="398"/>Si ke tuit m'esgarderent et paien et juï
<lb n="399"/>Et li mal et li bon ki moi orent servi.</q>
			    </ab>
			    <ab type="gv">
<lb n="400" rend="alinea"/><q>Nichodemus i ert, cui granz pitiez en prist.
<lb n="401"/>Cant li jors fut aleiz et il fut envesprit,
<lb n="402"/>Si revint a mon cors, les clas de fer fors prist,
<lb n="403"/>Si moi mist jus a terre; cant il m'ot recollit,
<lb n="404"/>Drezat moi devant soi, a engardeir me prist :
<pb n="68"/>
<lb n="405"/><q> “ Ohi, glorïos Deus! cum granz plaies at ci!
<lb n="406"/>Vos en ireiz en ciel, la dont za deschendiz.
<lb n="407"/>Or vos en vulh proier par la vostre mercit,
<lb n="408"/>Cant venreiz en vo regne, ramembre vos de mi. ”</q> »</q>
<lb n="409"/>Tuit avons parjuret, li grant et li petit,
<lb n="410"/>Et sa mort et ses plaies ke il por nos soffrit.
<lb n="411"/>Li espirs repairat, al tierc jor surrexit,
<lb n="412"/>Si en alat en ciel visiteir ses amis.
			    </ab>
			    <ab type="gv">
<lb n="413" rend="alinea"/>Or deprions tuit Deu par la sue mercit
<lb n="414"/>K'aleir puissons od lui a la joie senz fin,
<lb n="415"/>A la dextre partie avoc la sainte companie.
<lb n="416"/>Amen.
			    </ab>
			</div>		    
<pb n="69"/>
		    <div type="version" n="2">
<note resp="editor">B = <hi rend="ital">Paris, Bibl. Nat.,f.fr. 19525, fol. 46 r°b Variantes de
R = Manchester, The John Rylands Library, French 6, fol. 7vb</hi> </note> <milestone unit="folio" n="46rb"/>
		        <ab type="gv">
<lb n="413" rend="alinea"/>Ore depriun tuit Deu et la soue merci
<lb n="414"/>Et la sainte virgine ou Deu char et sanc prist
<lb n="415"/>Que sa joie aiun tuit devant lui al juïs,
<lb n="416"/>Cum cil del ciel quant virent Deu venir,
<lb n="417"/>Cumme s'aparut entre els et si dist: <q>« <foreign xml:lang="la">Pax vobis!</foreign>
<lb n="418"/>Beneit soiés vos el ciel tuit li mien ami
<lb n="419"/>Et cil qui sunt en terre et remembrent de mi. <milestone unit="folio" n="46va"/>
<lb n="420"/>Pais vos aport et joie qui dura tos dis.
<lb n="421"/>Revenu sui d'enfer, le doleros païs,
<lb n="422"/>Maint en ai fors mis qui erent en eissil,
<lb n="423"/>Ne cil qui la charunt arunt socors par mi,
<lb n="424"/>- Et Deu nos en defende par la soue merci -
<lb n="425"/>Puis repairai en terre et povre et mendif.
<lb n="426"/>Quant veneie a vos us, ne me vousistes veïr,
<lb n="427"/>Tant ne vos soi proier que en eussiés merci;
<lb n="428"/>En vos grans maisuns ne me lessastes gisir,
<lb n="429"/>Vos me meistes al plain as freis et as gresis,
<lb n="430"/>Sempres le troverez comme m'avez servi.
<lb n="431"/>Et vos, riches femmes que ci vei devant mi,
<lb n="432"/>Ou est ore le grant orgoil et le doleros pris,
<lb n="433"/>Les mantials traïnans, les tribolez sorplis,
<lb n="434"/>Et les goles de martre dunt eustes tel pris,
<lb n="435"/>Et les nusches d'or qui pendeient devant?
<lb n="436"/>Ou sunt les lasses mains as enials d'or lussant
<lb n="437"/>Et les mances de paile par terre traïnant?
<lb n="438"/>Qui adouba charoigne et or et argent i mist,
<lb n="439"/>Ne lui remembra mie de Dampnedeu servir,
<pb n="70"/>
<lb n="440"/>Ne la lasse d'alme secore ne garir,
<lb n="441"/>Vos prendrez penitence eneveis devant mi. »</q>
		        </ab>
		        <ab type="gv">		            
<lb n="442" rend="alinea"/>Et dunc respundrez : <q>« Bel sire, unc ne te vi,
<lb n="443"/>Ne tei ne tun mesage ne tun povre mendif.
<lb n="444"/>Si te veïsse, sire, si cumme tu dis ore ici,
<lb n="445"/>Donasse tei bons dras et pain et char et vin.
<lb n="446"/>Por la vostre amisté leis mun cors en eissil,
<lb n="447"/>Car nos ramaine al siecle, si ferun tun plaisir. »</q>
<lb n="448"/>Et Deu lor respundra : <q>« Ne l'avez deservi.
<lb n="449"/>Tant cumme fustes al siecle ne me vousistes veïr.
<lb n="450"/>Qui vostre servise firent, si irunt en eissil. »</q>
		        </ab>
		        <ab type="gv">
<lb n="451" rend="alinea"/>Par un dolent devendres iert li juïs, <milestone unit="folio" n="46vb"/>
<lb n="452"/>Si cumme la noit iert alee et li jors ert esclarciz,
<lb n="453"/>Dunc apara la crois ou Dampnedeu fu mis,
<lb n="454"/>Et Deu vendra aprés al terme qu'il i mist.
<lb n="455"/>Endreite ore de prime tuit ierent al juïs
<lb n="456"/>Et a oure de tierce tuit li saint altresi,
<lb n="457"/>Et a ore de midi ara Deu ses jugemens diz,
<lb n="458"/>Et a oure de none tuit ierent departiz,
<lb n="459"/>Et a oure de vespre li bon el champ flori.
		        </ab>
		        <ab type="gv">
<lb n="460"/>Dunc parlera nostre sire quant li plais ert finiz :
<lb n="461"/><q>« Alez li maleet, tolez vos de devant mi!
<lb n="462"/>Jeo vos requis el siecle, vos nen eustes merci.
<lb n="463"/>Maleït soiésvos, ici vos en defi
<lb n="464"/>De mun pere del ciel et de toz mes amis
<lb n="465"/>Et de la gloriouse virge ou jeo char et sanc pris
<lb n="466"/>Seez vos maleït et en enfer tot dis.
<pb n="71"/>
<lb n="467"/>Ore vos aient deables que vos avez servi,
<lb n="468"/>Ceo que vos ferunt vos estovra sosfrir.
<lb n="469"/>Et vos, li beneet, qui bien m'avez servi,
<lb n="470"/>Venez avant receivre le boneuré païs.
<lb n="471"/>Mostrerai vos la joie que vos desirez issi,
<lb n="472"/>Ne voudrez nule rien que n'aiés a tot dis.
<lb n="473"/>Ore entrez avant trestuit en paraïs,
<lb n="474"/>Chascun de vos ara ceo que a fait por mi. »</q>
		        </ab>
		        <ab type="gv">
<lb n="475" rend="alinea"/>Qui por lui fera bien, si l'ara tot dis,
<lb n="476"/>Tele joie ara qui Deu voudra servir.
<lb n="477"/>Dampnedeu nos prenge a sun oes a la fin.
<lb n="478"/>Ci finist le livre del Dampnedeu juïs.
<lb n="479"/>Deu nos prenge a sa part par la soue merci !
<lb n="480"/>Amen.
				</ab>
			</div>
		</body>
	</text>
</TEI>