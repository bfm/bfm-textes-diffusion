<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main">Espine</title>
      <title type="formal">espine</title>
      <title type="reference">espine</title>
      <title type="DEAF">EspineT</title>
      <title type="gmd">transcription électronique</title>
      <author xml:id="anonyme">anonyme</author>
      <editor>Équipe de la Base de français médiéval - ENS de Lyon / UMR 5317 IHRIM</editor>
      <funder>Laboratoire IHRIM</funder>
    </titleStmt>
    <editionStmt>
      <edition>transcripion électronique</edition>
      <respStmt xml:id="encoder0">
        <resp>Vérification de la description</resp>
        <name xml:id="AL">Alexei Lavrentiev</name>
      </respStmt>
      <respStmt xml:id="digitizer">
        <resp>scannage et océrisation</resp>
        <name xml:id="ES">Ekaterina Shcherbina</name>
      </respStmt>
      <respStmt xml:id="proofreader1">
        <resp>révision de tout le texte</resp>
        <name xml:id="PL">Pierre Levron</name>
      </respStmt>
      <respStmt xml:id="proofreader2">
        <resp>révision de tout le texte</resp>
        <name xml:id="VO">Vanessa Obry</name>
      </respStmt>
      <respStmt xml:id="encoderDD">
        <resp>balisage du discours direct</resp>
        <name xml:id="YM">Yunna Menshova</name>
      </respStmt>
    </editionStmt>
    <extent>- Numéros de pages saisies : de 264 à 283 ;
            - Nombre de mots d'après la base des descripteurs : <num xml:id="nb_mots_given">3005</num> ;
            - Nombre de tokens (mots et ponctuations) d'après la base des descripteurs : <num xml:id="nb_tokens_given">3466</num>.
    </extent>
    <publicationStmt>
      <publisher><name>Projet BFM, ENS de Lyon / UMR 5317 IHRIM</name></publisher>
      <address>
        <addrLine>15, parvis René Descartes</addrLine>
        <addrLine>69342 Lyon BP7000 Cedex 07</addrLine>
        <addrLine>Tél : 04 37 37 63 10</addrLine>
        <addrLine>Mèl : bfm@ens-lyon.fr</addrLine>
        <addrLine>http://bfm.ens-lyon.fr</addrLine>
      </address>
      <idno type="bfm">http://catalog.bfm-corpus.org/espine</idno>
      <idno type="doi">10.34847/nkl.80faksz3</idno>
      <availability status="free">
        <licence target="https://www.etalab.gouv.fr/licence-ouverte-open-licence">
          <ab type="availability_bfm" subtype="libre_1b">Les Conditions générales d'utilisation de la Base de Français Médiéval (http://txm.bfm-corpus.org/bfm/files/CGU.pdf), 
            et notamment son Article 5 « PROPRIETE INTELLECTUELLE », s'appliquent à tous les textes de la BFM.</ab>
          <ab type="how_to_cite">
            <label>Comment citer ce texte :</label>
            <bibl>Espine, édité par Prudence Mary O'Hara Tobin, Genève, 1976. 
              Publié en ligne par la Base de français médiéval, http://catalog.bfm-corpus.org/espine.
              Dernière révision le 2016-06-20.</bibl>
            D'autres formules de citation de la BFM sont proposées sur la page web suivante :
            http://bfm.ens-lyon.fr/article.php3?id_article=281.</ab>
          <ab type="availability_file">
            <label>Conditions d'utilisation de ce fichier :</label>
              Ce fichier numérique est composé du corps de texte médiéval et des suppléments numériques : balisage XML-TEI, métadonnées (teiHeader), balisage du discours direct. Le texte et les suppléments numériques sont libres de droit et peuvent à ce titre être téléchargés et réutilisés librement.
            </ab>
            <ab type="availability_txmweb" subtype="illimité">
              <label>Conditions d'utilisation par le portail BFM :</label>
              Pas de limitation dans l'usage des fonctionnalités. Téléchargement autorisé.</ab>
        </licence>
      </availability>
    </publicationStmt>
    <sourceDesc>
      <biblFull>
        <titleStmt>
          <title>Les lais anonymes : le lai de l'Espine</title>
          <respStmt>
            <resp xml:id="editor">Éditeur scientifique</resp>
            <name type="person">Prudence Mary O'Hara Tobin</name>
          </respStmt>
        </titleStmt>
          <extent>20 page(s), soit 264-282
        </extent>
        <publicationStmt>
          <publisher>Droz</publisher>
          <pubPlace>Genève</pubPlace>
          <date>1976</date>
          <idno type="sigle_DEAF">EspineT</idno>
          <idno type="cote_biblio">ENS-LSH : 84/2 A lai</idno>
        </publicationStmt><seriesStmt><title>Publications romanes et françaises</title><idno type="collection">143</idno></seriesStmt>
      </biblFull>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Projet :<name type="project">BFM - Base de français médiéval</name>
        Resp. : <name type="person">Céline Guillot-Barbance</name>
        Équipe : Base de français médiéval
        Laboratoire : UMR 5317 IHRIM
        Institution : ENS de Lyon
        <address>
          <addrLine>15, parvis René Descartes</addrLine>
          <addrLine>69342 Lyon BP7000 Cedex 07</addrLine>
          <addrLine>Tél : 04 37 37 63 10</addrLine>
          <addrLine>Mèl : bfm@ens-lyon.fr</addrLine>
          <addrLine>http://bfm.ens-lyon.fr</addrLine>
        </address>
      </p>
    </projectDesc>
    <editorialDecl>
      <p>Voir le <ref target="http://bfm.ens-lyon.fr/IMG/pdf/Manuel_Encodage_TEI.pdf">Manuel d'encodage XML-TEI de la BFM</ref>
        et les <ref target="http://bfm.ens-lyon.fr/IMG/pdf/Consignes_BFM.pdf">Consignes aux relecteurs de la BFM</ref>.</p>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="ab" occurs="5"/>
        <tagUsage gi="body" occurs="1"/>
        <tagUsage gi="choice" occurs="2"/>
        <tagUsage gi="corr" occurs="2"/>
        <tagUsage gi="div" occurs="2"/>
        <tagUsage gi="front" occurs="1"/>
        <tagUsage gi="lb" occurs="513"/>
        <tagUsage gi="num" occurs="11"/>
        <tagUsage gi="p" occurs="3"/>
        <tagUsage gi="pb" occurs="20"/>
        <tagUsage gi="q" occurs="13"/>
        <tagUsage gi="sic" occurs="2"/>
        <tagUsage gi="supplied" occurs="2"/>
        <tagUsage gi="text" occurs="1"/>
      </namespace>
    </tagsDecl>
  </encodingDesc>
  <profileDesc>
    <creation>
      <name type="author">anonyme</name>
      <title>Espine</title>
      <date type="compo" when="1190-01-01" notBefore="1180-01-01" notAfter="1200-01-01" n="12">vers la fin 12e s.</date>
      <date type="compo_periode">ancien</date>
      <date type="compo_sous_siecle" n="12_3">fin</date>
      <date type="ms" when="1285-01-01" notBefore="1280-01-01" notAfter="1290-01-01">1285 n.st.</date>
      <idno type="msIdentifier">Paris, BnF, fr. 1553 (B)</idno>
      <region type="dialecte_auteur">Nord-Ouest</region>
    </creation>
    <langUsage>
      <language ident="fro" usage="100">Le texte est entièrement écrit en vers en ancien français.</language>
    </langUsage>
    <textDesc>
      <channel mode="w">print</channel>
      <constitution type="single"/>
      <derivation type="original"/>
      <domain type="littéraire"/>
    <factuality type="inapplicable"/>
      <interaction type="none"/>
      <preparedness type="prepared"/>
      <purpose type="narrative"/></textDesc>
    <textClass>
      <keywords scheme="http://authorities.bfm-corpus.org">
        <term type="domaine">littéraire</term>
        <term type="genre">récit bref</term>
        <term type="forme">vers</term>
        <term type="relation">original</term>
      </keywords>
    </textClass>
  </profileDesc>
  <revisionDesc>
    <change who="#ES" when="2006-10-31">scannage et océrisation</change>
    <change who="#PL" when="2006-12-31">révision de tout le texte</change>
    <change who="#VO" when="2007-03-31">révision de tout le texte</change>
    <change who="#AL" when="2009-02-06">Vérification de la description</change>
    <change who="#YM" when="2016-06-20">balisage du discours direct</change>
    <change who="#AL" when="2016-06-20">balisage, numérotation et vérifications finales</change>
    <change when="2016-06-20">Dernière modification du texte</change>
    <change when="2022-11-04">Création de cet entête</change>
  </revisionDesc>
</teiHeader>
<text>
<pb n="264"/>
<front>
<div type="titre">
<p>CHI COMMENCHE LI LAIS DE L'ESPINE</p>
</div>
</front>
<body>
<div>
<ab type="gv">
<lb n="1"/>Qui que des lais tigne a mençoigne,
<lb n="2"/>saciés je nes tienc pas a songe ; 
<lb n="3"/>les aventures trespassees
<lb n="4"/>qui diversement ai contees,
<lb n="5"/>nes ai pas dites sans garant ;
<lb n="6"/>les estores en trai avant
<lb n="7"/>ki encore sont a Carlion
<lb n="8"/>ens el moustier Saint Aaron
<lb n="9"/>e en Bretaigne sont eües
<lb n="10"/>e en pluisors lius conneües.
<lb n="11"/>Por chou que les truis en memore,
<lb n="12"/>vos vuel demonstrer par estore
<lb n="13"/>de <num>.II.</num> enfans une aventure
<lb n="14"/>ki tous jors a esté obscure.
<pb n="265"/>
<lb n="15"/>En Bretaigne ot un damoisel, 
<lb n="16"/>preu e cortois e forment bel. 
<lb n="17"/>Nes de soignant, e fiex de roi, 
<lb n="18"/>pere e marastre ot desous soi. 
<lb n="19"/>Li rois l'ot cier que plus n'en ot, 
<lb n="20"/>e la roïne mout l'amot. 
<lb n="21"/>De l'autre part une meschine 
<lb n="22"/>d'autre signor ot la roïne ; 
<lb n="23"/>preus e cortoise ert la pucele, 
<lb n="24"/>e si estoit mout jovencele, 
<lb n="25"/>fille de roi e de roïne, 
<lb n="26"/>la coulor ot e bele e fine. 
<lb n="27"/>Andui furent de haut parage, 
<lb n="28"/>n'estoient pas de viel eage ; 
<lb n="29"/>li aisnés n'avoit que <num>.VII.</num> ans, 
<lb n="30"/>c'est cil ki estoit li plus grans. 
<lb n="31"/>Li doi enfant mout bel estoient ; 
<lb n="32"/>selonc l'entente qu'il avoient 
<lb n="33"/>volentiers ensanble juoient ; 
<lb n="34"/>en itel guise s'entramoient 
<lb n="35"/>que li uns d'aus riens ne valoit, 
<lb n="36"/>si li autres dalés n'estoit. 
<lb n="37"/>Ensi estoient, ce me sanble, 
<lb n="38"/>nourri trestout adés ensanble. 
<lb n="39"/>Ensamble aloient e juoient 
<lb n="40"/>e cil ki garder les devoient 
<lb n="41"/>de tout lor donnoient congié,
<pb n="266"/>
<lb n="42"/>ne lor faisoient nul fourkié,
<lb n="43"/>ne de boire ne de mangier,
<lb n="44"/>fors d'iax <num>.II.</num> ensanble couchier,
<lb n="45"/>mes cho ne leur est pas gréé.
<lb n="46"/>Tantost con furent de l'aé
<lb n="47"/>k'en soi le puist souffrir Nature,
<lb n="48"/>en bien amer misent lor cure ;
<lb n="49"/>si fu li enfantis amours
<lb n="50"/>k'il orent maintenu tous jors ;
<lb n="51"/>une autre amors i herbeja
<lb n="52"/>que Naturë i aporta.
<lb n="53"/>N'i a celui qui ne s'en sente,
<lb n="54"/>tout i ont mise lor entente
<lb n="55"/>de lor deduit a çou mener :
<lb n="56"/>en iax baisier e acoler.
<lb n="57"/>Tant les mena qu'al cief del tor
<lb n="58"/>les joinst ensanble cele amor,
<lb n="59"/>e tous li corages d'ariere
<lb n="60"/>lor torna en autre maniere ;
<lb n="61"/>conme cascuns plus s'aparçut
<lb n="62"/>de tant en iax l'amors plus crut.
<lb n="63"/>Mout s'entramoient loiaument ;
<lb n="64"/>s'il eüssent tel essïent
<lb n="65"/>de bien lor amors a garder
<lb n="66"/>con il orent en iax amer,
<lb n="67"/>a painnes fussent decheü,
<lb n="68"/>mais tost furent apercheü.
<lb n="69"/>Ensi avint que li dansiax
<lb n="70"/>ki tant estoit e preus e biax,
<pb n="267"/>
<lb n="71"/>est venus de riviere un jor,
<lb n="72"/>mal ot el cief por la calor.
<lb n="73"/>En une cambre a recelee
<lb n="74"/>por la noise e por la criee
<lb n="75"/>priveement ala couchier
<lb n="76"/>por un poi la painne abregier.
<lb n="77"/>En ses cambres ot la roïne
<lb n="78"/>ki mout bonement la doctrine,
<lb n="79"/>devant sa mere estoit sa drue.
<lb n="80"/>Si conme ele sot sa venue,
<lb n="81"/>ni atent per ne conpaignon,
<lb n="82"/>ne cele dist ni o ne non,
<lb n="83"/>en la cambre s'en vait tout droit,
<lb n="84"/>u ses amis el lit gisoit.
<lb n="85"/>Il l'a lïement recheüe,
<lb n="86"/>car el jour ne l'a plus veüe.
<lb n="87"/>Icele qui riens ne douta
<lb n="88"/>apriés lui el lit se coucha,
<lb n="89"/><num>.C.</num> fois le baise par douçour.
<lb n="90"/>Trop demeurent en la folour,
<lb n="91"/>car la roïne s'aparçoit ;
<lb n="92"/>en la cambre le sieut tout droit,
<lb n="93"/>mout soavet ses pas i atient,
<lb n="94"/>fermeüre ne le detient.
<lb n="95"/>La cambre trueve deffremee,
<lb n="96"/>eneslepas est ens entree
<lb n="97"/>e vait avant, ses a trovés
<pb n="268"/>
<lb n="98"/>la u gisent entracolés ; 
<lb n="99"/>l'amour connut tout en apert 
<lb n="100"/>de coi li uns a l'autre sert. 
<lb n="101"/>Mout fu dolante la roïne, 
<lb n="102"/>par le puing saisist la meschine 
<lb n="103"/>qu'ele laidist a cele fois, 
<lb n="104"/>apriés la mist en grant effrois, 
<lb n="105"/>e le tint en grant desepline, 
<lb n="106"/>mout sueffre painne la meschine. 
<lb n="107"/>Li damoisiaus remest dolens, 
<lb n="108"/>qant il oï les batemens, 
<lb n="109"/>la desepline e le casti 
<lb n="110"/>que sa mere fasoit por li. 
<lb n="111"/>Ne set que fache, ne que die, 
<lb n="112"/>bien set k'enfin ele est traïe, 
<lb n="113"/>e que il est del tout traïs, 
<lb n="114"/>car de tout est a li fallis. 
<lb n="115"/>De s'amie fu anguissous 
<lb n="116"/>e de l'uevre plus vergoignous ; 
<lb n="117"/>de la cambre n'ose issir fors, 
<lb n="118"/>a duel faire livre sen cors.
<lb n="119"/> <q>« Helas, fait il, que le ferai ? 
<lb n="120"/>Ja sans li vivre ne porai. 
<lb n="121"/>Diex, quel eurë e quel peciés ! 
<lb n="122"/>Con folement me sui gaitiés ! 
<lb n="123"/>Certes, se je ne rai m'amie,
<pb n="269"/>
<lb n="124"/>bien sai por li perdrai la vie. » </q>
<lb n="125"/>Endemetiers que le duel fait, 
<lb n="126"/>la roïnë au roi s'en vait, 
<lb n="127"/>ki jure e dist conme roïne 
<lb n="128"/>e bien se garde la meschine :
<lb n="129"/><q>« Que il o ma fille ne voist, 
<lb n="130"/>car autre cose ne li loist, 
<lb n="131"/>c'a ma fille ne voist parler ; 
<lb n="132"/>pensés de vostre fil garder. » </q>
<lb n="133"/>Li rois le varlet gardera
<lb n="134"/>en sa court garder le fera, 
<lb n="135"/>ensi seront bien desevré :
<lb n="136"/><q>« esgardés ke ce soit celé. » </q>
<lb n="137"/>Atant laissent lor parlement, 
<lb n="138"/>mais cil ki a duel faire entent, 
<lb n="139"/>por nule riens il ne demoure, 
<lb n="140"/>a sen pere vint a cele eure, 
<lb n="141"/>jentement le met a raison.
<lb n="142"/><q>« Sire, fait il, je quier un don. 
<lb n="143"/>Se de rien me volés aidier, 
<lb n="144"/>que vous me faites chevalier, 
<lb n="145"/>car aler veul en autre terre
<lb n="146"/>en saudees por pris conquerre. 
<lb n="147"/>Trop ai gaitié la cheminee, 
<lb n="148"/>s'en sai mout mains ferir d'espee. » </q>
<lb n="149"/>Li rois pas ne l'en escondist, 
<lb n="150"/>toute sa requeste li fist,
<pb n="270"/>
<lb n="151"/>puis li a dit que il sejourt 
<lb n="152"/>dedens un an, ens en sa court, 
<lb n="153"/>entretant sive les tornois 
<lb n="154"/>e gart les pas e les destrois.
<lb n="155"/>Or, avient sovent en la terre 
<lb n="156"/>aventure, ki le va querre. 
<lb n="157"/>Li damoisiaus li otroia, 
<lb n="158"/>qui escondire ne l'osa. 
<lb n="159"/>En la court remest o son pere, 
<lb n="160"/>e la meschinë o sa mere. 
<lb n="161"/>Mais endui si gardé estoient, 
<lb n="162"/>parler ensanble ne pooient, 
<lb n="163"/>ne de riens n'avoient loisir, 
<lb n="164"/>ne d'iax veoir, ne d'iax oïr 
<lb n="165"/>par mesage, ne par serjant ; 
<lb n="166"/>tant ala l'amors destraignant.
<lb n="167"/><num>.VIII.</num> jours devant le saint Jehan, 
<lb n="168"/>en meïsmë, en icel an 
<lb n="169"/>c'on fist del varler chevalier, 
<lb n="170"/>li rois est venus de cachier, 
<lb n="171"/>car ot prise a grant fuison 
<lb n="172"/>e volatile e venison. 
<lb n="173"/>La nuit qant vint aprés souper, 
<lb n="174"/>li rois s'asist por deporter 
<lb n="175"/>sor un tapis devant le dois, 
<lb n="176"/>ot lui maint chevalier cortois, 
<lb n="177"/>e ensanblë o lui ses fis. 
<lb n="178"/>Le lai escoutent d'Aiëlis 
<lb n="179"/>que uns Irois doucement note, 
<lb n="180"/>mout le sonnë ens en sa route.
<pb n="271"/>
<lb n="181"/>Apriés celi, d'autre conmenche, 
<lb n="182"/>nus d'iaus n'i noise ne n'i tenche ; 
<lb n="183"/>le lai lor sone d'Orpheÿ, 
<lb n="184"/>e qant icel lai ot feni, 
<lb n="185"/>li chevalier aprés parlerent, 
<lb n="186"/>les aventures raconterent 
<lb n="187"/>que soventes fois sont venues 
<lb n="188"/>e par Bretaigne sont veües. 
<lb n="189"/>Entr'iaus avoit une meschine ; 
<lb n="190"/>ele dist : au gué de l'Espine, 
<lb n="191"/>en la nuit de la saint Jehan, 
<lb n="192"/>en avenoit plus qu'en tout l'an, 
<lb n="193"/>mais ja nus chouars chevalier 
<lb n="194"/>cele nuit n'i iroit gaitier.
<lb n="195"/>Li damoisiaus ot e entent, 
<lb n="196"/>que mout ot en lui hardement, 
<lb n="197"/>sor cho que puis qu'il çainst l'espee, 
<lb n="198"/>n'ot il aventure trovee ; 
<lb n="199"/>or li estuet par hardieche 
<lb n="200"/>faire malvaistie ne proeche. 
<lb n="201"/>Apriés le conte, e la pucele, 
<lb n="202"/>le roi e les barons apiele, 
<lb n="203"/>e tuit loent petit e grant. 
<lb n="204"/><q>« Signor, fait il, a vos me vant 
<lb n="205"/>que la nuis dist la mescine 
<lb n="206"/>gaitera au gué de l'Espine
<pb n="272"/>
<lb n="207"/>e prendra illuec aventure 
<lb n="208"/>quels qu'ele soit, u povre u dure. » </q>
<lb n="209"/>Qant li rois l'ot, s'en ot pesance, 
<lb n="210"/>la parole tint a enfance.
<lb n="211"/><q>« Biax fils, dist il, lais ta folie. 
<lb n="212"/>Cil dist qu'il ne le laira mie, 
<lb n="213"/>mais toute voies i ira. 
<lb n="214"/>Qant li rois voit qu'il nel laira 
<lb n="215"/>ne l'en volt avant faire vié.</q>
<lb n="216"/><q>- Or tost, fait il, a Dieu congié, 
<lb n="217"/>e si soies preus e seürs,
<lb n="218"/>e Diex te doinse bons eürs. » </q>
<lb n="219"/>Cele nuit alerent cochier ; 
<lb n="220"/>ensi sueffre le chevalier 
<lb n="221"/>dessi que fu au seme jor. 
<lb n="222"/>S'amie fu en grant freor, 
<lb n="223"/>car bien ot oï noveler 
<lb n="224"/>que ses amis en dut aler. 
<lb n="225"/>Icele nuit fist a estrous 
<lb n="226"/>gaitier au gué aventurous. 
<lb n="227"/>E qant li jors trait vers le soir, 
<lb n="228"/>li chevaliers ot bon espoir ; 
<lb n="229"/>de toutes armes est armés, 
<lb n="230"/>sor un bon cheval est montés, 
<lb n="231"/>droit au gué de l'Espine vait. 
<lb n="232"/>E la damoisiele, ke fait ?
<pb n="273"/>
<lb n="233"/>Seule s'en entre en un vergier,
<lb n="234"/>por son ami vuolt a proier
<lb n="235"/>que sainc e saus Diex le ramaint.
<lb n="236"/>Giete un soupir e dont se plaint,
<lb n="237"/>puis s'est assise sor une ente,
<lb n="238"/>a soi meïsme se demente,
<lb n="239"/>e donques dist : <q>« Pere celestre
<lb n="240"/>se onques fu, ne ja puet estre,
<lb n="241"/>c'onques avenist orement
<lb n="242"/>e chou c'on prie a nule gent,
<lb n="243"/>par coi nus hom fust deshaitiés,
<lb n="244"/>biaux Sire, prenge t'en pitiés
<lb n="245"/>que li miens amis od moi fust
<lb n="246"/>e jou od lui, s'estre peüst.
<lb n="247"/>E Diex, conseroie garie ;
<lb n="248"/>nus ne set con j'ai dure vie,
<lb n="249"/>e nus savoir ne le poroit,
<lb n="250"/>fors sol ichil ki ameroit
<lb n="251"/>la riens qu'il n'avroit a nul fuer,
<lb n="252"/>mais cil le set trestout par cuer. »</q>
<lb n="253"/>Ensi parloit la damoisiele,
<lb n="254"/>e seoit sor l'erbe noviele.
<lb n="255"/>Assés fu quise e demandee, 
<lb n="256"/>mais ains ne pot estre trovee, 
<lb n="257"/>car ne l'i siet cose ki vive. 
<lb n="258"/>Tant est a s'amor ententive 
<lb n="259"/>e a plorer e a duel faire,
<pb n="274"/>
<lb n="260"/>li jors en vait, la nuis repaire, 
<lb n="261"/>e donques fu auques lassee, 
<lb n="262"/>desous l'ente fu akeutee. 
<lb n="263"/>Li cuers un petit li tressaut, 
<lb n="264"/>illuec s'en dormi por le chaut. 
<lb n="265"/>N'i ot pas dormi longement, 
<lb n="266"/>mais je ne sai confaitement, 
<lb n="267"/>qui de desous l'ente fu prise 
<lb n="268"/>e au gué de l'Espine mise, 
<lb n="269"/>la u ses amis ciers estoit ; 
<lb n="270"/>mais ne fu gaires k'il i soit, 
<lb n="271"/>car repairiés est a l'espine, 
<lb n="272"/>dormant i troeve la meschine. 
<lb n="273"/>Por la freor cele s'esvelle, 
<lb n="274"/>ne set u est, molt s'en mervelle. 
<lb n="275"/>Son cief couvri, grant paour a, 
<lb n="276"/>li chevaliers l'aseüra.
<lb n="277"/><q>« Diva, fait il, por nient t'esfroies, 
<lb n="278"/>se es cose ki parler doies ; 
<lb n="279"/>seürement parole a moi
<lb n="280"/>por seul tant que feme te voi ; 
<lb n="281"/>s'en Dieu as part, soies seure, 
<lb n="282"/>mais que me dïes t'aventure, 
<lb n="283"/>par quel guise e confaitement 
<lb n="284"/>tu venis chi si soutieument. » </q>
<lb n="285"/>La meschine l'aseüra, 
<lb n="286"/>ses sans li mut, se li menbra 
<lb n="287"/>qu'ele n'estoit pas el vergier ; 
<lb n="288"/>dont apiele le chevalier.
<lb n="289"/><q>« U sui ge dont ? »</q> fait la meschine.
<pb n="275"/>
<lb n="290"/><q>« Damoisiele, au gué de l'Espine 
<lb n="291"/>u il avient mainte aventure,
<lb n="292"/>une fois bone, autre fois dure.</q>
<lb n="293"/><q>- He ! Diex ! ce dist, con sui garie ; 
<lb n="294"/>Sire, j'ai esté vostre amie.
<lb n="295"/>Diex a oïe ma priere. »</q>
<lb n="296"/>Ce fu l'aventure premiere 
<lb n="297"/>que la nuit vint au chevalier. 
<lb n="298"/>S'amie le ceurt embracier, 
<lb n="299"/>e il aprés a pié descent, 
<lb n="300"/>entre ses bras souëf le prent, 
<lb n="301"/>par <num>.C.</num> fois baise la meschine 
<lb n="302"/>e puis l'asiet desous l'espine. 
<lb n="303"/>Cele li conte tout, e dist 
<lb n="304"/>conment el vergier s'endormit 
<lb n="305"/>e conment il fu desi la, 
<lb n="306"/>e conment dormant le trova.
<lb n="307"/>Qant il ot trestout escouté, 
<lb n="308"/>un regart fist oltre le gué 
<lb n="309"/>e voit venir un chevalier 
<lb n="310"/>lance levee por gerroier. 
<lb n="311"/>Ses armes sont toutes vermelles, 
<lb n="312"/>e del cheval les deus orelles, 
<lb n="313"/>e li autres cors fu tous blans, 
<lb n="314"/>bien fu estrois desos les flans ; 
<lb n="315"/>mais n'a mie passé le gué, 
<lb n="316"/>de l'autre part s'est arresté.
<lb n="317"/>E li dansiaus dist a s'amie 
<lb n="318"/>que faire vieut chevalerie,
<pb n="276"/>
<lb n="319"/>d'illuec se part, pas ne se mueve. 
<lb n="320"/>Saut el cheval, sa joste trueve, 
<lb n="321"/>mais primes pense lui aidier 
<lb n="322"/>de l'autre part au estrivier. 
<lb n="323"/>Tant con cheval püent randir 
<lb n="324"/>grans cols se vont entreferir 
<lb n="325"/>en son le vermés des escus, 
<lb n="326"/>que tous les ont frais e fendus ; 
<lb n="327"/>les hanstes furent de quartier ; 
<lb n="328"/>sans malmetre e sans empirier 
<lb n="329"/>se versent endui el sablon ; 
<lb n="330"/>n'i orent per ne conpaignon 
<lb n="331"/>qui les aidaist a remonter, 
<lb n="332"/>or penst cascuns del relever ; 
<lb n="333"/>li graviers fu plains e ingaus. 
<lb n="334"/>Qant il refurent as chevaus 
<lb n="335"/>les escus joingnent as poitrines, 
<lb n="336"/>e baiscent les lances franines. 
<lb n="337"/>Li damoisiax ot honte eue 
<lb n="338"/>qu'a tiere vint devant sa drue 
<lb n="339"/>a cele jouste premerainne. 
<lb n="340"/>Sel feri si a le demainne 
<lb n="341"/>que de l'escu porte les hiés, 
<lb n="342"/>e cil refiert lui tout adiés ; 
<lb n="343"/>des hanstes font les trons voler, 
<lb n="344"/>lequel que soit, estuet verser. 
<lb n="345"/>Ce fu cil a vermelles armes, 
<lb n="346"/>de l'escu guerpi les enarmes 
<lb n="347"/>e del corant destrier la siele. 
<lb n="348"/>Voiant les iex a la puciele
<pb n="277"/>
<lb n="349"/>ses amis l'espaint el gravier, 
<lb n="350"/>par le regne prent le destrier, 
<lb n="351"/>(el gué se met, outre s'en vet, 
<lb n="352"/>de l'autre part gesir le let. 
<lb n="353"/>A s'amie vint, a l'espine, 
<lb n="354"/>du bon cheval li fet sesine. 
<lb n="355"/>Cil n'i jut mie longuement, 
<lb n="356"/>car secors ot assez briement. 
<lb n="357"/>Vers lui vienent dui chevalier 
<lb n="358"/>monter le font en <num>.I.</num> destrier.)
<lb n="359"/>Icil dui passerent le gué. 
<lb n="360"/>Li dansiaus en fu effreé 
<lb n="361"/>por cho qu'il n'estoient pas per, 
<lb n="362"/>mais ne l'en estuet pas douter ; 
<lb n="363"/>ja uns n'avra de l'autre aïe 
<lb n="364"/>se faire vieut chevalerie ; 
<lb n="365"/>faire le puet cortoisement 
<lb n="366"/>e cascuns par soi simplement. 
<lb n="367"/>Qant a cheval furent tout troi, 
<lb n="368"/>cortoisement e sans desroi 
<lb n="369"/>le gué passent li premerain.
<lb n="370"/>Qant outre furent en ciertain 
<lb n="371"/>ne l'araisonent tant ne qant 
<lb n="372"/>mais de jouster li font sanblant. 
<lb n="373"/>Li uns d'iaus fu cois e riestis, 
<lb n="374"/>li autres est es armes mis.
<lb n="375"/>Courtoisement l'atent e biel 
<lb n="376"/>por avoir joste del dansiel. 
<lb n="377"/>Qant cil les voit de tel musure
<pb n="278"/>
<lb n="378"/>isnelepas se raseüre,
<lb n="379"/>e entretant s'est porpensé
<lb n="380"/>por cho vient il gaitier au gué :
<lb n="381"/>por pris e por honor conquerre.
<lb n="382"/>Le vassal est alés requerre,
<lb n="383"/>lance baissie, a l'escu pris,
<lb n="384"/>el gravier est contre lui mis.
<lb n="385"/>Andui por joindre ensanble <choice><corr resp="editor">m<supplied rend="word_part">e</supplied>urent</corr><sic>murent</sic></choice>,
<lb n="386"/>es lances andui se <choice><corr resp="editor">rech<supplied rend="word_part">e</supplied>urent</corr><sic>rechurent</sic></choice>,
<lb n="387"/>si que des lances font astieles
<lb n="388"/>mais ne vuidierent pas les sieles.
<lb n="389"/>Tant furent fort li chevalier,
<lb n="390"/>aquastroné sont li destrier,
<lb n="391"/>e cascuns a mis pié a tiere,
<lb n="392"/>ot les bons brans se vont requerre.
<lb n="393"/>Ja fu li caples commenciés,
<lb n="394"/>e si fust li uns d'iaus bleciés
<lb n="395"/>qant li chevaliers les depart
<lb n="396"/>ki lons estoit a une part ;
<lb n="397"/>d'iax <num>.II.</num> desoivre la mellee,
<lb n="398"/>n'i ot plus colp feru d'espee.
<lb n="399"/>Puis a parlé au damoisiel,
<lb n="400"/>courtoisement li dist e bel :
<lb n="401"/><q>« Amis, fait il, car retornés
<lb n="402"/>e une fois a moi joustés,
<lb n="403"/>puis nous em porons bien aler,
<lb n="404"/>ne nous caut de plus demorer,
<pb n="279"/>
<lb n="405"/>car la painne de cest trepas 
<lb n="406"/>vous ne le soufferïés pas 
<lb n="407"/>ains que li jours doit esclarcir 
<lb n="408"/>par toute la cité de Tir ; 
<lb n="409"/>e se vous estïés malmis 
<lb n="410"/>e par mesaventure ocis, 
<lb n="411"/>vostre pris ariés vous perdu, 
<lb n="412"/>ja ne seriés amenteü. 
<lb n="413"/>Nus ne saroit vostre aventure, 
<lb n="414"/>ains seroit a tous jors oscure ; 
<lb n="415"/>menee en seroit la pucele 
<lb n="416"/>od le boin destrier de Castiele 
<lb n="417"/>que avois conquis par proeche. 
<lb n="418"/>Ains mais ne vistes tel richece 
<lb n="419"/>car, tant que le frains li lairois, 
<lb n="420"/>ja mar que mangier li donrois, 
<lb n="421"/>e tous jors l'arois cras e biel, 
<lb n="422"/>ainc mais ne vistes plus isniel. 
<lb n="423"/>Mais ne soiés ja esbahis 
<lb n="424"/>por cho qu'estes preus e hardis, 
<lb n="425"/>puis que le frain l'avrois tolu, 
<lb n="426"/>isnelement l'avrois perdu. » </q>
<lb n="427"/>Li dammoisiax ot et entent 
<lb n="428"/>qu'il parole raisnablement, 
<lb n="429"/>e se c'est voirs que li destine
<pb n="280"/>
<lb n="430"/>aler en vuet a la meschine. 
<lb n="431"/>Mais primes vuet a lui joster, 
<lb n="432"/>plus biel pora de lui sevrer, 
<lb n="433"/>avec les armes prent le regne 
<lb n="434"/>e prent une lanche de fraisne, 
<lb n="435"/>eslongiés s'est del chevalier 
<lb n="436"/>e prendent le cors el gravier.
<lb n="437"/>Pour asanbler ensanble poignent, 
<lb n="438"/>les lances baissent et eslongent. 
<lb n="439"/>Desor les escus a argent 
<lb n="440"/>s'entrefierent si fierement 
<lb n="441"/>que tous les ont frais e fendus, 
<lb n="442"/>mais les estriers n'ont pas pierdus. 
<lb n="443"/>E qant se sont si bien tenu, 
<lb n="444"/>si l'a li damoisiaus feru 
<lb n="445"/>que tous en fust venus aval, 
<lb n="446"/>qant au col se pent del cheval. 
<lb n="447"/>E li varlers outre s'en passe, 
<lb n="448"/>son escu e sa lanche quasse, 
<lb n="449"/>son tour fait, cele part s'adrece, 
<lb n="450"/>e li chevaliers se redrece, 
<lb n="451"/>au repairier tout prest le trueve, 
<lb n="452"/>cascuns de son escu se cuevre 
<lb n="453"/>e il ont traites les espees. 
<lb n="454"/>Si se donnent mout grans colees 
<lb n="455"/>que de lor escus font astieles 
<lb n="456"/>mais ne vuidierent pas les sieles. 
<lb n="457"/>Mout fu la mescinë effree
<pb n="281"/>
<lb n="458"/>qu'adiés regarde la mellee, 
<lb n="459"/>grand paor a de son ami, 
<lb n="460"/>au chevalier crie merchi 
<lb n="461"/>que a lui a jousté avant 
<lb n="462"/>que il s'ens départist atant. 
<lb n="463"/>Il fu cortois e afaitiés, 
<lb n="464"/>cele part vint tous eslaisciés, 
<lb n="465"/>de illuec departi se sont, 
<lb n="466"/>l'aighe passent, si se revont, 
<lb n="467"/>e li dansiaus plus ne demeure, 
<lb n="468"/>od s'amie vint enesleure - 
<lb n="469"/>paoureuse est desor l'espine - 
<lb n="470"/>devant soi lieve la meschine. 
<lb n="471"/>Le boin cheval en destre enmainne, 
<lb n="472"/>or a achevie sa painne. 
<lb n="473"/>Tant a erré que vint au jor 
<lb n="474"/>e vint a la cort son signor. 
<lb n="475"/>Li rois le voit e fu mout liés, 
<lb n="476"/>mais de chou s'est il mervelliés. 
<lb n="477"/>E cil a prise la mescine - 
<lb n="478"/>sire est, endroit soi la roïne. 
<lb n="479"/>Cel jor si con j'oï conter 
<lb n="480"/>a fait li rois sa cort mander 
<lb n="481"/>e ses barons e autre gent 
<lb n="482"/>por le droit d'un conmandement 
<lb n="483"/>de <num>.II.</num> barons ki se mellerent
<pb n="282"/>
<lb n="484"/>e devant le roi s'acorderent. 
<lb n="485"/>Oiant toute cele asanblee 
<lb n="486"/>li fu l'aventure contee : 
<lb n="487"/>conment avint au chevalier 
<lb n="488"/>au gué u il ala gaitier ; 
<lb n="489"/>premierement de la meschine 
<lb n="490"/>qu'il la trova desous l'espine, 
<lb n="491"/>puis des joustes e del cheval 
<lb n="492"/>que il gaaigna au vassal. 
<lb n="493"/>Li chevaliers, e pres e loing, 
<lb n="494"/>le mena puis en maint besoing 
<lb n="495"/>e richement garder le fist, 
<lb n="496"/>e la meschine a feme prist. 
<lb n="497"/>Tant garda e tint le destrier 
<lb n="498"/>que la dame volt assaier
<lb n="499"/>ce c'est du cheval verité 
<lb n="500"/>que son signor a tant gardé, 
<lb n="501"/>le frain del cief li a tolu, 
<lb n="502"/>ensi ot le cheval pierdu. 
<lb n="503"/>De l'aventure que dit ai, 
<lb n="504"/>li Breton en fisent un lai 
<lb n="505"/>por chou qu'elë avint au gué 
<lb n="506"/>n'ont pas li Breton esgardé 
<lb n="507"/>que li lais recheüst son non, 
<lb n="508"/>ne fu se de l'Espine non. 
<lb n="509"/>Ne l'ont pas des enfans nomé, 
<lb n="510"/>ains l'ont de l'Espine apielé, 
<lb n="511"/>s'a a non li lais de l'Espine
<pb n="283"/>
<lb n="512"/>qui bien conmenche e biel define. 
<lb n="513"/>Chi define li lais de l'Espine.
				</ab>
</div>
</body>
</text>
</TEI>