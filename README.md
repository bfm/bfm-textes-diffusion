# BFM Textes Diffusion


## FR
Cet entrepôt contient les textes des corpus publics de la [Base de français médiéval](http://txm.bfm-corpus.org) aux différents formats. Les données sont diffusées sous la [Licence Ouverte Etalab](https://www.etalab.gouv.fr/licence-ouverte-open-licence/), à l'exclusion de l'apparat critique disponible sous licence [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/) pour les fichiers XML-TEI suivants : AlexisRaM, ChaceOisiiT, eulaliBfm, ImMondePrK, PsArundP, qgraal_cm, QJoyesKa, strasbBfm.xml. Les répetoires suivants correspondent aux formats diffusés :

- CONLL-U : Fichiers au format [CONLL-U](https://universaldependencies.org/format.html) étiquetés en morphosyntaxe et éventuellement lemmatisés :
    - BFMGOLDLEM : textes lemmatisés et étiquetés ;
    - BFMGOLDPOS : tous les textes de BFMGOLDLEM + les textes étiquetés mais non lemmatisés ;
- TEI-TXM : Fichiers au format XML TEI étendu pour la plateforme TXM. Tous les mots sont balisés &lt;tei:w&gt;, les propriétés de mots sont encodées dans les sous-éléments &lt;txm:ana&gt;. Voir la [Spécification](https://groupes.renater.fr/wiki/txm-info/public/xml_tei_txm) pour plus de détails ;
- TEIP5-BFM : Fichiers au format XML TEI P5. Voir l'[ODD](http://bfm.ens-lyon.fr/spip.php?article326) ;
- TXT : Textes au format texte brut.


## EN

This repository contains the texts of the public corpora of the [Base de Français Médiéval Old French Corpus](http://txm.bfm-corpus.org) in various formats. All data are available under an [Etalab Open Licence](https://www.etalab.gouv.fr/licence-ouverte-open-licence/) with the exception of critical apparatus available under a [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/) license in the following TEI XML files: AlexisRaM, ChaceOisiiT, eulaliBfm, ImMondePrK, PsArundP, qgraal_cm, QJoyesKa, strasbBfm.xml. The following directories correspond to the distributed formats:

- CONLL-U : [CONLL-U](https://universaldependencies.org/format.html) formatted files with manually verified POS tags and/or lemmas:
    - BFMGOLDLEM : lemmatized and pos-tagged texts;
    - BFMGOLDPOS : all BFMGOLDLEM texts + pos-tagged non lemmatized texts;
- TEI-TXM: Files in TEI XML format extended for TXM Platform. All the words are tagged with &lt;tei:w&gt;, the word properties (annotations) are encoded as &lt;txm:ana&gt; sub-elements. See the [Specification](https://groupes.renater.fr/wiki/txm-info/public/xml_tei_txm) for more details.
- TEIP5-BFM : Files in XML TEI P5 format. See the [ODD](http://bfm.ens-lyon.fr/spip.php?article326) (in French);
- TXT: Simple text files.
